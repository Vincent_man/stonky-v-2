### ASH-AVILA : Project Manager

## 3/28:

ICE-BREAKER - SUPERPOWER(ash):
Adrian -
Alissa - Invisibility
Ash - Invisibility
Vincent -

ASH:
Worked - Issues & Template / FastAPI Research
Working - Issues - writing and completing
Blocker - Pipeline Issue

## 3/29:

ICE-BREAKER - FAMOUS PERSON HANG(ash):
Adrian - CEO TikTok
Alissa - Van Gogh
Ash - Sue Bird

ASH:
Worked - Exploration & FastAPI Research, got table up, built line-by-line def table for yaml file
Working - Building Assets table
Blocker - n/a

## 3/30:

ICE-BREAKER - FAVE ANIMAL(alissa):
Adrian - Capybara
Alissa - Leopard Seal
Ash - Hippo

ASH:
Worked - Got Assets Table Up
Working - FastAPI not pulling table data
Blocker - Model Issue

## 4/3:

ICE-BREAKER - One meal forever?:
Adrian - Noodles
Alissa - Spam Musubi
Ash - Mexican Food
Vincent - Italian

ASH:
Worked - Completed assets backend functions
Working - journals and auth
Blocker - n/a

## 4/4:

ICE-BREAKER - What do your fam & friends think you do all day?:
Adrian - Instructions for humans to communication with computers
Alissa - Gma = build internet, bro = video game designer, rest = stare at screen all day
Ash - Build the internet
Vincent - They are generally confused

ASH:
Worked - auth & Assets pulled down / approved a merge
Working - finish auth & backend
Blocker - remote branch, solved

## 4/5:

ASH:
Stock API to be finish first
I dont care what components, everyone feels like workload is even

## 4/6:

ICE-BREAKER - What’s your hidden talent?:
Adrian - is a chef
Alissa - has a magical green thumb
Ash - Imma locksmith
Vincent - Magician!!

ASH:
Worked - finished 3rd party api
Working - take front end base page + component +journaling/issues
Blocker - mapping blocker but got it

## 4/17:

ICE-BREAKER - What did you do over the break for yourself?:
Adrian - fam time and movie w/ partner (dino sci fi movie)
Alissa - Ate all the foods, sleeped all the sleeps
Ash - Sun nap

ASH:
Worked - Redux
Working - Merge code - start hero container - journal and issues
Blocker - all of the things, payload typo

## 4/19:

ICE-BREAKER - What song makes you smile?
Adrian - Mr. Brightside : killers
Alissa - Teenagers : MCR
Ash - you gotta be : des’ree OR Taylor Swift
Vincent - Politics & Violence : Dominic Fike

ASH:
Worked - hero container & css
Working - nav bar and form css
Blocker - ticker tape wont work!

## 4/20:

ASH:
Worked - hero container & css
Working - ui design for full page
Blocker - ticker tape wont work!

## 4/21:

ASH:
Worked - full page uidesign
Working - component ui design
Blocker - ticker tape wont work!

## 4/24:

ICE-BREAKER - What’s the last thing you did for the first time?
Adrian - made strawberry mochi
Alissa -
Ash - hosted a seder
Vincent -

ASH:
Worked - deployed design updated, passed token to nav & built a delete all assets for resetting account
Working - design for rest of pages after getting all components merged
Blocker - n/a

## 4/25:

ASH:
Worked - did design layout for all pages including animated scroll, designed working components
Working - design and popup edit form
Blocker - none

## 4/26:

ASH:
Worked - got account page and edit form design sorted
Working - implementing design for all components and finding fun component for front page - journaling
Blocker - none

## 4/27:

ICE-BREAKER - If you had to sum yourself up professionally in one word, what would it be?
Adrian - Reliable
Alissa - Humble
Ash - Detail Oriented

ASH:
Worked - design and last animation piece - unit test
Working - finish animation and education
Blocker - none


