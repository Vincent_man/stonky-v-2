# Alissa's Journal Entries!


## 3/28:
---
ICE-BREAKER - SUPERPOWER(ash):


Alissa - Invisibility

Ash - Invisibility



ALISSA

Worked - Issues / FastAPI Research

Working - Issues

Blocker - Personal

## 3/29:
---
ICE-BREAKER - FAMOUS PERSON HANGOUT

Adrian - CEO TikTok

Alissa - Van Gogh

Ash - Sue Bird

Vincent - Sam Altman



ALISSA:

Worked - Exploration & FastAPI Udemy

Working - Trades Table

Blocker - N/A


## 3/30:
---
ICE-BREAKER - FAVE ANIMAL(alissa):

Adrian - Capybara

Alissa - Leopard Seal

Ash - Hippo

Vincent - Red Tail Hawk

ALISSA:

Worked - Finished Issues

Working - Update Issue Detail / Working

Blocker - N/A


## 4/3:
---
ICE-BREAKER - One meal forever?:

Adrian - Noodles

Alissa - Spam Musubi

Ash - Mexican Food

Vincent - Italian

ALISSA:

Worked - Studied Auth (Resources in resource channel)

Working - Finish up trades table, look into trade backend functions

Blocker - Personal


## 4/4:
---
ICE-BREAKER - What do your fam & friends think you do all day?:

Adrian - Instructions for humans to communication with computers

Alissa - Gma = build internet, bro = video game designer, rest = stare at screen all day

Ash - Build the internet

Vincent - They are generally confused

ALISSA:

Worked - Studied Backend Auth yet again for a clear understanding, selfcare

Working - Finish up my part of backend, understanding front end auth, go over existing code and troubleshoot

Blocker - Personal


## 4/5:
---
ICE-BREAKER - Whats an easy item on your bucket list?:

Adrian - build customer camper van

Alissa - Flip over a shark OR run from a bear

Ash - Hot tub in the snow

ALISSA:

Worked -

Working - FRONT END PLAN

Blocker - N/A



## PLAN:
---
ADRIAN:
Stock API as group
No specific request for component
Would like to check in on code as things get done
Start with frame and then add items

ALISSA:
Frontend request carousel & hero container, cool with anything else
Cool to start wherevs

ASH:
Stock API to be finish first
I dont care what components, everyone feels like workload is even

VINCENT:
Open to whatever first, market API is chill
Skeleton up first and then components per page


## 4/6:
---
ICE-BREAKER - What’s your hidden talent?:

Adrian - is a chef

Alissa - has a magical green thumb

Ash - Imma locksmith

Vincent - Magician!!


ALISSA:

Worked - finished 3rd party api with team

Working - take front end base page + component +journaling/issues

Blocker - mapping blocker but got it


## 4/7:
---
ICE-BREAKER - Which movie made you laugh hard?:

Adrian - Kevin Hart

Alissa - Haunted House

Ash - Superbad

Vincent - Scary Movie would be better with Tom Segura


ALISSA:

Worked - Updating Gitlab Issues

Working - Finishing updating Gitlab Issues

Blocker - N/A


## 4/17:
---
ICE-BREAKER - What did you do over the break for yourself?:

Adrian - fam time and movie w/ partner (dino sci fi movie)

Alissa - Ate all the foods, sleeped all the sleeps

Ash - Sun nap

ALISSA:

Worked - Skeletons for Profile

Working - Will finish up those components

Blocker - N/A


## 4/18:
---
ALISSA:

Worked - Finished components, add to nav

Working - Trying to implement fetching data into trade history and portfolio holdings page.

Blocker - N/A


## 4/19:
---
ICE-BREAKER - What song makes you smile?

Adrian - Mr. Brightside : killers

Alissa - Teenagers : MCR

Ash - you gotta be : des’ree OR Taylor Swift

Vincent - Politics & Violence : Dominic Fike


ALISSA:

Worked - Finished base components, Added all base components to App.js.

Working - Trying to implement fetching data into trade history and portfolio holdings page, try to get components to show up in react. (Profile components)

Blocker - N/A



## 4/20:
---
ICE-BREAKER - How would you change your life today if the average life expectancy was 400 years?

Adrian - would be doing something fun and takin out loans to fund

Alissa - would do all the things that are risky, flipping sharks/chasing bears

Ash - id probably still be acting super young, blowing money and traveling

Vincent - would be making bad decisions for another 70 years



ALISSA:

Worked - Finished base of components, Added all components to App.js.

Working - Fixing those dead buttons! Prioritizing the delete button (Want to finish by today), after the delete button will prioritize fetching data into portfolio trade history and the holding page. Once I finish those I will move onto the reset button. Creating my own due date of finishing all of those by the end of friday.

Blocker - Reset button and delete button aren't working



## 4/24:
---
ICE-BREAKER - What’s the last thing you did for the first time?

Adrian - made strawberry mochi

Alissa - pet a fish

Ash - hosted a seder

Vincent - N/A



ALISSA:

Worked - Delete button (Didn’t get it working)

Working - Different endpoint (TradeHistory on trade page), Need to create a tradeAPI

Blocker - N/A





## 4/25:
---
ALISSA:

Worked - Created TradeAPI, tweaked TradeAPI

Working - Working on finishing up TradeHistory on TradePage

Blocker - Need to test tradehistory make sure tradehistory works (have code already)





## 4/26:
---
ALISSA:

Worked - Created TradeAPI, tweaked TradeAPI yet again and created a trade slice

Working - Working on finishing up TradeHistory on TradePage (day2), created a unit test for get_all_trades need to make sure unit test runs

Blocker - Need trade history to pull a users trades




## 4/27:
---
ICE-BREAKER - If you had to sum yourself up professionally in one word, what would it be?

Adrian - Reliable

Alissa - Humble

Ash - Detail Oriented

Vincent - N/A


ALISSA:

Worked - Finished the ReadME files, worked on trade history need fix AAA, changed up tradesSlice and tradesAPI, all finished now!

Working - Need test trade history, trade history works no error but it isn’t giving back users trades (I lied after 1-2 hours I realized I forgot to captialize a letter in my code now trade history works perfect)

Blocker - N/A
