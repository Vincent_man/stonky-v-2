3/29:
VINCENT:
Worked - Exploration & Creating Tables
Working -
Blocker - n/a
3/30:
VINCENT:
Worked - Working User Table
Working - Got table viewed, but no data
Blocker - Errors in pulling table data
4/3:
VINCENT:
Worked - finished user endpoints
Working - front end components for users endpoints
Blocker - N/a
4/4:
VINCENT:
Worked - added auth to create user(with group), started testing market api and charting component.
Working - updating other user endpoints with auth, finishing back end.
Blocker - ran into issues with npm install for our charting library, but we believe we have a solution.
4/5:
VINCENT:
Worked -
Working - FRONT END PLAN
Blocker -
4/6:
VINCENT:
Worked - finished 3rd party api
Working - take front end base page + component +journaling/issues
Blocker - mapping blocker but got it
4/7:
VINCENT:
Worked - worked on merging tables as a group
Working - updating gitlab issues, moving into front end
Blocker - big blocker today, decided to do most of our joins on the front end
4/17:
VINCENT:
Worked - worked on creating pages and component skeletons for all trading page components
Working - charting component/stock detail view
Blocker - big blocker today, decided to do most of our joins on the front end
4/18:
VINCENT:
Worked -worked on creating separate component pages
Working - working getting buy sell windows to work, need to update to using update asset, and creating redux apis for assets/trades
Blocker - creating redux api for assets
4/19:
VINCENT:
Worked - finished functional buy sell window for trading page
Working - updating window to use update assets rather than only create/sell
Blocker - N/A
4/20:
VINCENT:
Worked - updating window to use update assets rather than only create/sell
Working - updating window to use update assets rather than only create/sell
Blocker - 1 unprocessable entity
4/24:
VINCENT:
Worked - updating trading page with search bar for fetch stock data parsing and passing the useful data to different trading components
Working - passing and tracking stock data from search to buy sell
Blocker - AHHHHHHHH
4/25:
VINCENT:
Worked - passing and tracking stock data from search to buy sell
Working - passing and tracking stock data from search to buy sell, if not finished by end of day, move into working this feature out of the project.
Blocker - passing and tracking stock data from search to buy sell
4/26:
VINCENT:
Worked - passing and tracking stock data from search to buy sell
Working - passing and tracking stock data from search to buy sell, if not finished by end of day, move into working this feature out of the project.
Blocker - passing and tracking stock data from search to buy sell
4/27:
VINCENT:
Worked - passing and tracking stock data from search to buy sell
Working -
Blocker - N/A