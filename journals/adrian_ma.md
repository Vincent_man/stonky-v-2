### Adrian Ma Journals

## 3/28:
ADRIAN:
Worked - Issues / FastAPI Research
Working - Trading Model
Blocker - Stock API Incorporation


Yesterday, I started writing the issues we need to complete as a team and started reviewing/going over lecture on FastAPI. Vincent and I spent some time figuring out how to incorporate the 3rd party stock market API in the frontend. Today, I'm planning to start writing the trading model for the backend.

## 3/29:
ADRIAN:
Worked - Exploration & FastAPI YouTube
Working - Portfolio Table
Blocker -

Yesterday, I started write trading model but got stuck. I was able to figure out out by following along with the fastAPI documention and learn material. Today, I plan to start writing the postgres sql data tables for Portfolio. We will also look into beekeeper and setting that up.

## 3/30:
ADRIAN:
Worked - Got FastAPI Table Up/ Finished Issues
Working - Portfolio Table Research
Blocker - Need to get data in table

Yesterday, we ran into some issues figuring out how beekeeper works but we were able to get it setup and pulling our data. Today, I will be continuing to wring the table for portfolio.


## 4/3:
ADRIAN:
Worked - Started Authentication
Working - created endpoints for login/logout tokens and create account endpoint
Blocker -  Issue retrieving access token

Last Friday, we decided we didn't need the Portfolio table because it was a bunch of redundent information for the backend. I spent the weekend reviewing auth material and started writing the authentication. I ran into a black where i was unable to retrieve the access token and decided to wait for my team to help figure it out. Today, I will start writing the endpoints for login/logout tokens and create account endpoint.

## 4/4:
ADRIAN:
Worked - Finished user_auth as a group, created merge request
Working - continuing auth for assets and trade
Blocker -  N/A

Yesterday, we were able to figure out user auth as a group and we completed a merge request. Today, plan on writing auth to protect our endpoints for assests and trades.


## 4/5:
ADRIAN:
Worked - Completed protecting our assets and trades endpoint.
Working - FRONT END PLAN
Blocker -

Yesterday, we were able to figure out how to protect our assets and trades endpoints as a team. We still start planning how we want our front end pages to look and what type of information we want to display. We will most likely try to draw this out on excalidraw.



## 4/6:
ADRIAN:
Worked - added foreign keys to assets/trade tables and updated endpoints for trade and assets to include user_id.
Working - take front end base page + component +journaling/issues
Blocker - n/a

Last night when mapping out how we want our frontend to look, I realized that we did not have a way to connect our user to assets and trades. I added user_id to both assets and trades tables referencing to our user table. We plan on splitting up the frontend components and updating our journals.



## 4/7:
ADRIAN:
Worked - completed and tested all backend endpoints and auth.
Working - starting to build front end components
Blocker -

We finalized and tested that all our backend endpoints are functioning last night. I plan on creating the nav bar component today and starting the profile page.


## 4/17:
ADRIAN:
Worked - Nav Bar up, edit profile form up
Working - Edit form put request
Blocker - fetching data and update feature

Yesterday,  I was able to complete the nav bar. It is functioning and able to redirect the user to the page they want to go. I also started working on the edit profile form for the profile page. Today, I will continue to work/debug the profile edit form.


## 4/18:
ADRIAN:
Worked - started deployment completed part1 and started part2
Working - continue deployment and edit form functionality
Blocker -

I started and completed parts 1 and part2 of deployment with Rosheen's notion notes. Today, I will continue and try to get part 3 and 4 working.


## 4/19:
ADRIAN:
Worked - attempted part 3 and 4 of deployment
Working - fix bugs and finish deployment
Blocker - ran into some bugs with deployment

Last night, I ran into some issues with part 3 and 4 of deployment. I will be trying to work through debug today and create a hmu if I can't figure it out.

## 4/20:
ADRIAN:
Worked - still working on deployment/review redux
Working - try and finish redux
Blocker - bugs with deployment/NGINX 502 error

Yesterday, I was able to get my pipeline to pass but my gitlab and caprover was still not communicating with each other. Today, I am going to try get some help from a seir to help unblock my NGINX 502 error and finish deployment.

## 4/24:
ADRIAN:
Worked -  deployment up and running, edit account form function with redux, delete account button working with redux
Working - create ledger tables and have it display necessary information. Create a parent node for the profile page displaying all profile page components
Blocker -

I was finally able to fully deploy our site using caprover. I started over from scratch and it worked, i might have overlooked or skipped a step in my first deployment attempt. I was also able to get my profile edit form working with redux and completed the delete button. Today, I will be working on the ledger form for the profile page.


## 4/25:
ADRIAN:
Worked -  editprofileform working with redux, moved delete button into edit form, started ledger form, created profile parent that contains all profile components
Working - finish ledger form
Blocker -  n/a

No blockers so far, I ended up moving the delete button into the edit profile page component, I was able to get the skeleton up for the ledger and create a profile parent component to put the editprofileform and the ledgerform together. Today, i will be fixing any bugs and testing all our components.


## 4/26:
ADRIAN:
Worked -  fixed all last minute bugs and errors for code freeze.
Working - finish ledger form, unit test
Blocker -  n/a

Fixed the bugs we are aware of, we did discover an issue where we aren't able to delete a user because it is tied to assets and trades but we made a work around. After grading on friday, we will go back and edit our postgres sql tables to include OnCascade so this issue wont occur. Today, i will finish the ledger form and start unit test.



## 4/27:
ADRIAN:
Worked -  implimented a balance calculator in ledger form. did final merge last night to prepare for deployment.
Working - Deployment as a team
Blocker -  n/a

Last night, I was able to get the ledger form to work and also implimented a balance calculator that shows you the remaining balance you have after purchasing/selling stocks. Today, we will be handling deployment as a team and fixing any bugs that show up as we deploy.


