import React from 'react';

function TradeLeaderboard({ traders }) {
  return (
    <div className="trade-leaderboard">
      <h2>Trade Leaderboard</h2>
      <table>
        <thead>
          <tr>
            <th>Rank</th>
            <th>Trader</th>
            <th>Recent Trades</th>
            <th>Profit/Loss</th>
          </tr>
        </thead>
        <tbody>
          {traders.map((trader, index) => (
            <tr key={trader.id}>
              <td>{index + 1}</td>
              <td>{trader.name}</td>
              <td>{trader.recentTrades.join(', ')}</td>
              <td>{trader.profitLoss}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
}

export default TradeLeaderboard;