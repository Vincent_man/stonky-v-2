import { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import { useSignupMutation } from "../store/authAPI";
import "react-toastify/dist/ReactToastify.css";
import stonkynew from "../images/stonkynew.png";
import { ToastContainer, toast } from "react-toastify";


function SignupForm({ onClose }) {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [signup, result] = useSignupMutation();
  const navigate = useNavigate();

  const handleRegistration = async (e) => {
    e.preventDefault();
    await signup({
      name,
      email,
      password,
      username,
    });
    e.target.reset();
  };

  useEffect(() => {
    if (result.isSuccess) {
      toast(`You're all set, ${username}!`);
      navigate('/');
      onClose();
    } else if (result.isError) {
      toast(`Something went wrong...`);
      result.reset();
    }
  });



  return (
    <>
      <div className="form-container site-form">
        <div className="close-btn" onClick={onClose}>
          X
        </div>
        <h5 className="card-header">
          <img alt="" src={stonkynew} />
        </h5>
        <div className="card-body">
          <form onSubmit={handleRegistration}>
            <div className="mb-3">
              <label className="form-label">Username</label>
              <input
                name="username"
                type="text"
                className="form-control"
                onChange={(e) => {
                  setUsername(e.target.value);
                }}
              />
            </div>
            <div className="mb-3">
              <label className="form-label">Password</label>
              <input
                name="password"
                type="password"
                className="form-control"
                onChange={(e) => {
                  setPassword(e.target.value);
                }}
              />
            </div>
            <div className="mb-3">
              <label className="form-label">Name</label>
              <input
                name="name"
                type="text"
                className="form-control"
                onChange={(e) => {
                  setName(e.target.value);
                }}
              />
            </div>
            <div className="mb-3">
              <label className="form-label">Email</label>
              <input
                name="email"
                type="text"
                className="form-control"
                onChange={(e) => {
                  setEmail(e.target.value);
                }}
              />
            </div>
            <button className="site-btns">Create Account</button>
          </form>
        </div>
      </div>
      <ToastContainer
        position="bottom-right"
        autoClose={2000}
        hideProgressBar={false}
        newestOnTop
        closeOnClick
        rtl={false}
        pauseOnFocusLoss={false}
        draggable
        pauseOnHover={false}
        theme="dark"
        z-index="999999"
      />
    </>
  );
}

export default SignupForm;
