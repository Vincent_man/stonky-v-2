// import useToken from "@galvanize-inc/jwtdown-for-react";
import { useState } from "react";
import { useLoginMutation } from "../store/authAPI";
import { useGetTokenQuery } from "../store/authAPI";
import stonkynew from "../images/stonkynew.png";
import { ToastContainer, toast } from "react-toastify";



const Login = ({onClose}) => {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [login, result] = useLoginMutation();

  const { data } = useGetTokenQuery();
  if (data === undefined) {
    return null;
  };


  const handleSubmit = async (e) => {
    e.preventDefault();
    await login({ username, password });
    e.target.reset();
    toast(`Welcome Back. ${username}!`);
  };

  // useEffect(() => {
  //   if (result.isSuccess) {
  //     navigate("/");
  //     onClose();
  //   } else {
  //     return null
  //   }
  // }, [result.isSuccess, navigate, onClose]);


  return (
    <>
      <div className="form-container site-form">
        <div className="close-btn" onClick={onClose}>
          X
        </div>
        <h5 className="card-header">
          <img alt="" src={stonkynew} />
        </h5>
        <div className="card-body">
          <form onSubmit={(e) => handleSubmit(e)}>
            <div className="mb-3">
              <label className="form-label">Username:</label>
              <input
                alt=""
                name="username"
                type="text"
                className="form-control"
                onChange={(e) => setUsername(e.target.value)}
              />
            </div>
            <div className="mb-3">
              <label className="form-label">Password:</label>
              <input
                alt=""
                name="password"
                type="password"
                className="form-control"
                onChange={(e) => setPassword(e.target.value)}
              />
            </div>
            <div>
              <button className="site-btns"> Login </button>
            </div>
          </form>
        </div>
        {result.isError && <div>LOGIN FAILED</div>}
      </div>
      <ToastContainer
        position="bottom-right"
        autoClose={2000}
        hideProgressBar={false}
        newestOnTop
        closeOnClick
        rtl={false}
        pauseOnFocusLoss={false}
        draggable
        pauseOnHover={false}
        theme="dark"
        z-index="999999"
      />
    </>
  );
};

export default Login;
