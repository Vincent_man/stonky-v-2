import React, { useState, useEffect } from "react";

import { useGetTokenQuery } from "../store/authAPI";
import { useGetTradeQuery } from "../store/tradesAPI";
import Popup from "reactjs-popup";

function LedgerForm({ onClose }) {
  let data = undefined;
  let account = undefined;
  let userTrades = undefined;
  let [balance, setBalance] = useState(1000000);
  // let [balance] = useState();
  const { data: alltrades, isLoading: alltradesloading } = useGetTradeQuery();
  const { data: tokendata, isLoading } = useGetTokenQuery();
  if (!isLoading && !alltradesloading) {
    data = alltrades;
    account = tokendata.account.id;
    userTrades = data.filter((trade) => trade.users_id === account);
  }

  useEffect(() => {
    // eslint - disable - line;
    setBalance(balance); // eslint - disable - line;
  }, [account]); //eslint-disable-line

  if (account !== undefined) {
    return (
      <div className="d-flex justify-content-center user-ledger">
        <div className="form-container m-3">
          <div className="mb-3 ">
            <div className="edu-container">
              <Popup
                trigger={
                  <div className="edu-btn" onClick={onClose}>
                    ?
                  </div>
                }
                position="right top"
              >
                <div>
                  Trades refer to the buying and selling of securities such as
                  stocks, bonds, and options. In the stock market, a trade is
                  executed when a buyer agrees to purchase a certain number of
                  shares at a specific price from a seller. Trades can occur
                  between individual investors or can be facilitated by a broker
                  or an exchange.
                  <p>
                    When an investor decides to buy or sell a stock, they place
                    an order with their broker or an online trading platform.
                    The order specifies the security to be traded, the price at
                    which the investor wants to buy or sell, and the quantity of
                    shares to be traded. Once the order is placed, the broker or
                    platform will try to match the order with a counterparty who
                    is willing to buy or sell the same security at the specified
                    price.
                  </p>
                  <p>
                    When a trade is executed, it is recorded and reported to the
                    stock exchange where the security is traded. Trades can be
                    executed in real-time during market hours or can be
                    scheduled for a later time if the investor chooses a limit
                    order.
                  </p>
                  <p>
                    Trades can be classified into different types, such as
                    market orders, limit orders, stop orders, and options
                    orders. Market orders are executed at the current market
                    price, while limit orders specify a maximum or minimum price
                    at which the investor is willing to buy or sell. Stop orders
                    are used to limit losses by selling a stock at a specific
                    price if it falls below a certain level. Options orders are
                    used to buy or sell options contracts.
                  </p>
                  <p>
                    Trades are an important aspect of the stock market and allow
                    investors to buy and sell securities in a timely and
                    efficient manner. The volume of trades on a stock exchange
                    is a key indicator of market activity and can affect the
                    price and liquidity of the securities being traded.
                  </p>
                </div>
              </Popup>
            </div>
            <h2 className="">Your Trades:</h2>
            <table className="table">
              <thead>
                <tr>
                  <th>Bank/Cash Balance:</th>
                  <th></th>
                  <th>Transaction Date:</th>
                  <th>Buy/Sell</th>
                  <th>Stock</th>
                  <th>Shares</th>
                  <th>Price per Share</th>
                  <th>Total Spent/Total Earned:</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td rowSpan="1000">${balance}</td>
                </tr>
                {userTrades.map((trade) => {
                  balance = trade.purchase_price
                    ? balance - parseInt(trade.purchase_price)
                    : balance + parseInt(trade.sold_price);
                  return (
                    <tr key={trade.id}>
                      <td></td>
                      <td>{trade.purchase_date}</td>
                      <td>{trade.purchase_price ? "Buy" : "Sell"}</td>
                      <td>{trade.ticker}</td>
                      <td>{trade.amount}</td>
                      <td>${trade.cost_basis}</td>
                      <td>
                        ${trade.purchase_price}
                        {trade.sold_price}
                      </td>
                    </tr>
                  );
                })}
              </tbody>
            </table>
          </div>
        </div>
      </div>
    );
  }
}

export default LedgerForm;
