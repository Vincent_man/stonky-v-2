import React, { useState } from "react";
import Popup from "reactjs-popup";
import LedgerForm from "./LedgerForm";
import EditAccountForm from "./ProfileEditForm";
import { useGetTokenQuery } from "../store/authAPI";
import { useDeleteTradeMutation } from "../store/tradesAPI";
import { useDeleteUserMutation } from "../store/usersAPI";
import { useLogoutUserMutation } from "../store/authAPI";
import { useDeleteAssetMutation } from "../store/assetsAPI";
import { useNavigate } from "react-router-dom";
import Fullpage, {
  FullPageSections,
  FullpageSection,
  FullpageNavigation,
} from "@ap.cx/react-fullpage";
import graph from "../images/smallleft.jpg";
import window from "../images/toppurpgreen.jpg";
import reset from "../images/bottom.jpg";
import stonky from "../images/stonkynew.png";
import "../Site.css";
import AssetTable from "./AssetsTable";
import { ToastContainer, toast } from "react-toastify";

function ProfileParentForm() {
  const { data, isLoading } = useGetTokenQuery();
  const [deleteAllTrades] = useDeleteTradeMutation();
  const [deleteAllAssets] = useDeleteAssetMutation();
  const navigate = useNavigate();

  const handleDeleteAllMoney = async (e) => {
    e.preventDefault();
    await deleteAllTrades(data.account.id);
    await deleteAllAssets(data.account.id);
    alert(`Time to start fresh!`);
  };

  const [deleteUser] = useDeleteUserMutation();
  const [deleteTrade] = useDeleteTradeMutation();
  const [deleteAsset] = useDeleteAssetMutation();

  const [logoutUser] = useLogoutUserMutation();
  const [showModal, setShowModal] = useState(false);

  if (isLoading) {
    return <progress className="progress is-primary" max="100"></progress>;
  }

  function openForm() {
    setShowModal(true);
  }

  function closeForm() {
    setShowModal(false);
  }
  const accountDelete = data.account.id;
  const account = data.account;

  const handleDelete = async () => {
    try {
      const a = await deleteAsset(accountDelete);
      if (a.data === true) {
        const b = await deleteTrade(accountDelete);
        if (b.data === true) {
          deleteUser(accountDelete);
          await logoutUser();
          navigate("/");
        }
      }
    } catch (error) {
      toast(`User could not be deleted.`);
    }
  };

  const SectionStyle = {
    height: "100vh",
    width: "100%",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
  };

  return (
    <>
      <Fullpage>
        <FullpageNavigation />
        <FullPageSections>
          <div className="d-flex profile">
            <div className="parent-prof">
              <FullpageSection
                style={{
                  ...SectionStyle,
                  backgroundImage: `url(${window})`,
                  backgroundSize: "cover",
                  dsiplay: "flex",
                  flexDirection: "column",
                }}
              >
                <div className="profile-container m-3">
                  <div className="profile-window">
                    <div className=" mb-3">
                      <label
                        htmlFor="exampleInputEmail1"
                        className="form-label"
                      >
                        Email:
                      </label>
                      <div className="form-label-field">{account.email}</div>
                    </div>
                    <div className="mb-3">
                      <label
                        htmlFor="exampleInputEmail1"
                        className="form-label"
                      >
                        Name:
                      </label>
                      <div className="form-label-field">{account.name}</div>
                    </div>
                    <div className="mb-3">
                      <label
                        htmlFor="exampleInputEmail1"
                        className="form-label"
                      >
                        Username:
                      </label>
                      <div className="form-label-field">{account.username}</div>
                    </div>
                  </div>
                  <img className="parent-img" src={stonky} alt="Profile" />
                </div>
                <div className="profile-btn-container">
                  <button className="profile-btns" onClick={openForm}>
                    Edit
                  </button>
                  {showModal ? (
                    <div className="modal-backdrop">
                      <div className="modal-content">
                        <EditAccountForm onClose={closeForm} />
                      </div>
                    </div>
                  ) : null}
                  <button className="profile-delete" onClick={handleDelete}>
                    Delete
                  </button>
                </div>
              </FullpageSection>
              <FullpageSection
                style={{
                  ...SectionStyle,
                  backgroundImage: `url(${graph})`,
                  backgroundSize: "cover",
                  dsiplay: "flex",
                  flexDirection: "column",
                }}
              >
                <LedgerForm />
                <AssetTable />
              </FullpageSection>
              <FullpageSection
                style={{
                  ...SectionStyle,
                  backgroundImage: `url(${reset})`,
                  backgroundSize: "cover",
                  dsiplay: "flex",
                  flexDirection: "column",
                }}
              >
                {" "}
                <Popup
                  trigger={
                    <div>
                      <h1>DONT DO IT!!</h1>
                      <h2>Click For Info</h2>
                    </div>
                  }
                  position="center"
                >
                  <div className="reset-popup-info">
                    This will delete all of the trading history associated with
                    your username.
                  </div>
                </Popup>
                <button
                  className="reset-btn"
                  href="#"
                  onClick={handleDeleteAllMoney}
                >
                  Reset Account Assets
                </button>
              </FullpageSection>
            </div>
          </div>
        </FullPageSections>
      </Fullpage>
      <ToastContainer
        position="bottom-right"
        autoClose={2000}
        hideProgressBar={false}
        newestOnTop
        closeOnClick
        rtl={false}
        pauseOnFocusLoss={false}
        draggable
        pauseOnHover={false}
        theme="dark"
        z-index="999999"
      />
    </>
  );
}

export default ProfileParentForm;
