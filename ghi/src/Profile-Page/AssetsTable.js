import React from "react";
import { useGetTokenQuery } from "../store/authAPI";
import { useGetAssetsQuery } from "../store/assetsAPI";
import Popup from "reactjs-popup";

function AssetTable(onClose) {
  let users_id = undefined;
  const { data, isLoading } = useGetTokenQuery();
  if (!isLoading) {
    users_id = data.account.id;
  }
  const users_assets = [];

  const GetAssets = () => {
    var a = useGetAssetsQuery();
    var as = a.data;
    if (as !== undefined) {
      for (let a of as) {
        if (a.users_id === users_id) {
          users_assets.push(a);
        }
      }
    }
  };
  GetAssets();
  const assets = users_assets.slice(-6);
  console.log(assets);
  //   useEffect(() => {

  //   }, []);

  if (users_id !== undefined) {
    return (
      <>
        <div className="d-flex justify-content-center user-ledger">
          <div className="form-container m-3">
            <div className="mb-3 ">
              <div className="edu-container">
                <Popup
                  trigger={
                    <div className="edu-btn" onClick={onClose}>
                      ?
                    </div>
                  }
                  position="right top"
                >
                  <div>
                    Assets are economic resources that a company or an
                    individual owns or controls, which have the potential to
                    generate future economic benefits. In other words, an asset
                    is anything that a company or individual owns that has value
                    and can be used to generate income or other benefits in the
                    future.
                    <p>
                      Examples of assets include physical assets such as real
                      estate, equipment, inventory, and vehicles. Intangible
                      assets, such as patents, copyrights, trademarks, and
                      goodwill, are also considered assets. Investments in other
                      companies, such as stocks and bonds, are also assets.
                    </p>
                    <p>
                      Assets are typically listed on a company's balance sheet
                      and are classified into different categories such as
                      current assets and non-current assets. Current assets are
                      those that are expected to be used or converted into cash
                      within a year, such as inventory and accounts receivable.
                      Non-current assets, also called fixed assets, are those
                      that are expected to be used for more than a year, such as
                      buildings and equipment.
                    </p>
                    <p>
                      The value of an asset is usually its original cost, less
                      any depreciation or amortization, although some assets may
                      appreciate in value over time. A company's or an
                      individual's assets are an important measure of their
                      financial health and can be used to obtain loans or other
                      forms of financing.
                    </p>
                  </div>
                </Popup>
              </div>
              <h2>Your Assets</h2>
              <table className="table">
                <thead>
                  <tr>
                    <th>Ticker</th>
                    <th>Shares</th>
                    <th>Price Per Share</th>

                    <th>Date</th>
                    <th>Total Value</th>
                  </tr>
                </thead>
                <tbody>
                  {assets.map((event) => (
                    <tr key={event.id}>
                      <td>{event.ticker}</td>
                      <td>{event.amount}</td>
                      <td>${parseInt(event.cost_basis).toFixed(2)}</td>

                      <td>{event.purchase_date}</td>
                      <td>
                        ${parseInt(event.cost_basis) * parseInt(event.amount)}
                      </td>
                    </tr>
                  ))}
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </>
    );
  }
}

export default AssetTable;
