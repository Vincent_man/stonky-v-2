import React from "react";
import { useDeleteUserMutation } from "../store/usersAPI";
import { useGetTokenQuery } from "../store/authAPI";

function DeleteAccount() {
  const token = useGetTokenQuery();
  const account = token.data.account.id;
  const [deleteUser] = useDeleteUserMutation();

  const handleDelete = async () => {
    try {
      await deleteUser({ users_id: account });
    } catch (error) {
      console.error(error)
    }
  };

  return (
    <button className="delete_button" onClick={handleDelete}>
     delete
    </button>
  );
}

export default DeleteAccount;
