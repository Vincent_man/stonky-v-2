import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import { useEditUserMutation } from "../store/authAPI";
import { useGetTokenQuery } from "../store/authAPI";
import { useDeleteUserMutation } from "../store/usersAPI";
import { useLogoutUserMutation } from "../store/authAPI";
import { useDeleteTradeMutation } from "../store/tradesAPI";
import { useDeleteAssetMutation } from "../store/assetsAPI";

function EditAccountForm({ onClose }) {

  const [editUser] = useEditUserMutation();
  const [deleteUser] = useDeleteUserMutation();
  const [deleteTrade] = useDeleteTradeMutation();
  const [deleteAsset] = useDeleteAssetMutation();
  const [logoutUser] = useLogoutUserMutation();
  const navigate = useNavigate();

  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");

  let account = undefined;
  let user = undefined;
  const { data, isLoading } = useGetTokenQuery();
  if (!isLoading) {
    account = data.account.id;
    user = data.account;
  }

  const handleEdit = async (event) => {
    event.preventDefault();

    const result = await editUser({
      users_id: account,
      data: {
        name: name,
        email: email,
        password: password,
        username: username,
      },
    });

    if (!result.hasOwnProperty("error")) {
      setName(name);
      setEmail(email);
      setPassword(password);
      setUsername(username);
    }
    logoutUser();
    navigate("/");
  };

  const accountDelete = data.account.id;
  const handleDelete = async () => {
    try {
      const a = await deleteAsset(accountDelete);
      if (a.data === true) {
        const b = await deleteTrade(accountDelete);
        if (b.data === true) {
          deleteUser(accountDelete);
          await logoutUser();
          navigate("/");
        }
      }
    } catch (error) {
      console.error(error);
      alert("user delete failed");
    }
  };


  if (user !== undefined) {
    return (
      <>
        <div className="d-flex justify-content-center edit-form">
          <div className="form-container m-3">
            <div className="close-btn" onClick={onClose}>
              X
            </div>
            <form onSubmit={handleEdit}>
              <div className="form-label-title tw-text-2xl">
                Edit Account:
              </div>
              <div className="mb-3">
                <label htmlFor="exampleInputEmail1" className="form-label">
                  Email:
                </label>
                <input
                  className="form-control"
                  defaultValue={email}
                  onChange={(event) => setEmail(event.target.value)}
                  placeholder="New Email"
                />
              </div>
              <div className="mb-3">
                <label htmlFor="exampleInputEmail1" className="form-label">
                  Name:
                </label>
                <input
                  className="form-control"
                  defaultValue={name}
                  onChange={(event) => setName(event.target.value)}
                  placeholder="New Name"
                />
              </div>
              <div className="mb-3">
                <label htmlFor="exampleInputEmail1" className="form-label">
                  Username
                </label>
                <input
                  className="form-control"
                  defaultValue={username}
                  onChange={(event) => setUsername(event.target.value)}
                  placeholder="New Username"
                />
              </div>
              <div className="mb-3">
                <label htmlFor="exampleInputEmail1" className="form-label">
                  Password
                </label>
                <input
                  type="password"
                  className="form-control"
                  defaultValue={password}
                  onChange={(event) => setPassword(event.target.value)}
                  placeholder="New Password"
                />
              </div>
              {/* <div className="mb-3">
            <label htmlFor="exampleInputEmail1" className="form-label">
              Choose Skill Level
            </label>
            <select
              className="form-select"
              placeholder="user skill level will go here"
              readOnly
            >
              <option>Beginner</option>
              <option>Intermediate</option>
              <option>Advanced</option>
            </select>
          </div> */}

              <button
                type="submit"
                className="profile-btns"
                onClick={handleEdit}
              >
                Edit
              </button>
              <button
                type="submit"
                className="profile-delete"
                onClick={handleDelete}
              >
                Delete
              </button>
            </form>
          </div>
        </div>
      </>
    );
  }
}

export default EditAccountForm;
