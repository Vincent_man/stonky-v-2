import { React } from "react";
import Nav from "./Nav";
import { Routes, Route, BrowserRouter } from "react-router-dom";
// import { AuthProvider } from "@galvanize-inc/jwtdown-for-react";
import DeleteAccount from "./Profile-Page/DeleteAccountButton";
import ProfileParentForm from "./Profile-Page/ProfileParent";
import LandingPage from "./Landing-Page/LandingPage.js";
import TradeHistory from "./Portfolio/PortfolioTradeHistory";
import TradingPage from "./Trading-Page/TradingPage.js";
import MyPlot from "./Trading-Page/ChartingComponet";
import Login from "./Users/Login";
import SignUp from "./Users/SignUp";
import Test from "./Trading-Page/Test.js"
import TradingPageV2 from "./Trading-Page/TradingPageV2";
import TradingPageNu from "./Nu-Tra-Pg/Trading-Page-Parent";
import BuySell from "./Nu-Tra-Pg/BuySell";
import Skeleton from "./Nu-Tra-Pg/skeleton";
import ToggleSwitch from "./Nu-Tra-Pg/BuySellToggle";
import "./test.css";
// import "./Site.css";
// import green_bg from "./videos/greenbg.mp4";

function App() {
  const domain = /https:\/\/[^/]+/;
  const basename = process.env.PUBLIC_URL.replace(domain, "");

  return (
    <>
      <BrowserRouter basename={basename}>
        {/* <Nav /> */}

          <Routes path="/">
            <Route path="" element={<LandingPage />} />
            <Route path="trading" element={<TradingPage />} />
            <Route path="tradehistory" element={<TradeHistory />} />
            <Route path="login" element={<Login />} />
            <Route path="signup" element={<SignUp />} />
            <Route path="trading" element={<TradingPage />} />
            {/* <Route path="chart" element={<MyPlot />} /> */}
            <Route path="deleteaccount" element={<DeleteAccount />} />
            <Route path="profilepage" element={<ProfileParentForm />} />
            <Route path="t" element={<TradingPageNu/>} />
            <Route path="buy" element={<BuySell/>} />
            <Route path="ske" element={<Skeleton/>} />
            <Route path="tog" element={<ToggleSwitch/>} />
          </Routes>

      </BrowserRouter>
    </>
  );
}

export default App;
