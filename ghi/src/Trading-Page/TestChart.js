import React from "react";
import Plot from "react-plotly.js";
import Popup from "reactjs-popup";

function Chart({ high, low, Today, OneYearAgo, Dates, ticker, onClose }) {
  const trace1 = {
    type: "scatter",
    mode: "lines",
    name: `${ticker} High`,
    x: Dates,
    y: high,
    line: { color: "#00FF00" },
  };
  const trace2 = {
    type: "scatter",
    mode: "lines",
    name: `${ticker} Low`,
    x: Dates,
    y: low,
    line: { color: "#FF0000" },
  };

  const layout = {
    title: `${ticker} Preformance`,
    xaxis: {
      range: [OneYearAgo, Today],
      type: "date",
    },
    yaxis: {
      autorange: true,
      range: [100, 300],
      type: "linear",
    },
  };
  class App extends React.Component {
    render() {
      return (
        <>

            <div className="">
              <div className="">
                <Plot data={[trace1, trace2]} layout={layout} />
              </div>
            </div>

        </>
      );
    }
  }
  return <App />;
}
export default Chart;
