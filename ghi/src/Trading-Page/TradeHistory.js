import { React, useEffect, useState } from "react";
// import { useSelector } from "react-redux";
// import { useGetTradeQuery } from "../store/tradesAPI";
// import { useGetTokenQuery } from "../store/authAPI";
import Popup from "reactjs-popup";

function TradeHistory({ buyEvents, sellEvents, tradeHistory, onClose }) {
  // let users_id = undefined;
  // const { data, isLoading } = useGetTokenQuery();
  // if (!isLoading) {
  //   users_id = data.account.id;
  // }
  // const [tradehistory, setTradeHistory] = useState([]);
  const [buyevents, setBuyEvents] = useState([]);
  const [sellevents, setSellEvents] = useState([]);

  useEffect(() => {
    if (tradeHistory.length > 0) {
      var recentBuy = buyEvents.slice(-6); // get the last 6 items from the array
      var recentSell = sellEvents.slice(-6); // get the last 6 items from the array
      // setTradeHistory(tradeHistory);
      setBuyEvents(recentBuy);
      setSellEvents(recentSell);
    }
  }, [tradeHistory, buyEvents, sellEvents]);

  // if (users_id !== undefined) {
    return (
      <>
        <div className="d-flex justify-content-center tw-z-[9999] tw-absolute">
          <div className="trade-history">
            <h1>Trade History</h1>
            <div>
              <h2>Buy:</h2>
            <table className="table">
              <thead>
                <tr>
                  <th>Ticker</th>
                  <th>Shares</th>
                  <th>Price</th>
                  <th>Date</th>
                  <th>Total</th>
                </tr>
              </thead>
              <tbody>
                  <tr>
                    <td>tdtdtd</td>
                    <td>tdtdtd</td>
                    <td>tdtdtd</td>
                    <td>tdtdtd</td>
                    <td>tdtdtd</td>
                  </tr>
              </tbody>
            </table>
          </div>
          <div>
            <h2>Sell:</h2>
            <table className="table">
              <thead>
                <tr>
                  <th>Ticker</th>
                  <th>Shares</th>
                  <th>Price</th>
                  <th>Date</th>
                  <th>Total</th>
                </tr>
              </thead>
              <tbody>
                  <tr>
                    <td>tdtdtd</td>
                    <td>tdtdtd</td>
                    <td>tdtdtd</td>
                    <td>tdtdtd</td>
                    <td>tdtdtd</td>
                  </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </>
  );
}
// }

export default TradeHistory;
