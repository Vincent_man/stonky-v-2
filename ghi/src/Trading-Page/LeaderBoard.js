import { React, useEffect } from "react";

function LeaderBoard() {

  useEffect(() => {}, []);

  return (
    <>
      <div className="d-flex justify-content-center">
        <div className="leaderboard">
          <h1 className="">Leaderboard</h1>

          {/* this will pull all users portfolio preformance for a given time peroid
                    and select the top preforming users to display here */}
          {/* define as stretch goals */}
          <table className="table">
            <thead>
              <tr>
                <th>Username</th>
                <th>Rank</th>
                <th>Profit</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>Adrian</td>
                <td>1</td>
                <td>$1,230,450</td>
              </tr>
              <tr>
                <td>Alissa</td>
                <td>2</td>
                <td>$923,450</td>
              </tr>
              <tr>
                <td>Ash</td>
                <td>3</td>
                <td>$823,450</td>
              </tr>
              <tr>
                <td>Vincent</td>
                <td>4</td>
                <td>$723,450</td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </>
  );
}

export default LeaderBoard;
