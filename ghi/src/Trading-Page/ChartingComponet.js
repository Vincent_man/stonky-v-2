// import React from "react";
// import Plot from "react-plotly.js";
// import Popup from "reactjs-popup";

// function Chart({ x, y, high, low, Today, OneYearAgo, Dates, ticker, onClose }) {
//   // const OneMonthToToday = () => {
//   //   const today = new Date();
//   //   const year = today.getFullYear();
//   //   const month = String(today.getMonth() + 1).padStart(2, "0");
//   //   const day = String(today.getDate()).padStart(2, "0");
//   //   const formattedToday = `${year}-${month}-${day}`;

//   //   const oneMonthAgo = new Date();
//   //   oneMonthAgo.setMonth(oneMonthAgo.getMonth() - 1);

//   //   const year1 = oneMonthAgo.getFullYear();
//   //   const month1 = String(oneMonthAgo.getMonth() + 1).padStart(2, "0");
//   //   const day1 = String(oneMonthAgo.getDate()).padStart(2, "0");
//   //   const formattedOneMonthAgo = `${year1}-${month1}-${day1}`;

//   //   return [formattedToday, formattedOneMonthAgo];
//   // };

//   // function getDateRange(startDate, endDate) {
//   //   var dates = [];
//   //   var currentDate = new Date(startDate);
//   //   endDate = new Date(endDate);

//   //   while (currentDate <= endDate) {
//   //     dates.push(new Date(currentDate).toISOString().slice(0, 10));
//   //     currentDate.setDate(currentDate.getDate() + 1);
//   //   }
//   //   return dates;
//   // }

//   const trace1 = {
//     type: "scatter",
//     mode: "lines",
//     name: `${ticker} High`,
//     x: Dates,
//     y: high,
//     line: { color: "#00FF00" },
//   };
//   const trace2 = {
//     type: "scatter",
//     mode: "lines",
//     name: `${ticker} Low`,
//     x: Dates,
//     y: low,
//     line: { color: "#FF0000" },
//   };

//   const layout = {
//     title: `${ticker} Preformance`,
//     xaxis: {
//       range: [OneYearAgo, Today],
//       type: "date",
//     },
//     yaxis: {
//       autorange: true,
//       range: [100, 300],
//       type: "linear",
//     },
//   };
//   class App extends React.Component {
//     render() {
//       return <Plot data={[trace1, trace2]} layout={layout} />;
//     }
//   }

//   return (
//     <>
//       <div className="d-flex justify-content-center">
//         <div className="charting">
//           <div className="form-container site-form m-3">
//             <div className="mb-3">
//               <label className="form-label-title">Chart:</label>
//               <Popup
//                 trigger={
//                   <div className="edu-btn" onClick={onClose}>
//                     ?
//                   </div>
//                 }
//                 position="bottom"
//               >
//                 <div>
//                   Ticker Performance:{" "}
//                   <p>
//                     The overall performance of a particular stock over a given
//                     period. A stock's ticker symbol is a unique series of
//                     letters that is used to identify it on a stock exchange. By
//                     tracking the performance of a stock using its ticker symbol,
//                     investors can get an idea of how the stock is performing in
//                     the market. Ticker performance is often measured by looking
//                     at a stock's price movement over a particular period. This
//                     can be done by comparing the stock's current price to its
//                     price at a previous point in time, such as its opening
//                     price, closing price, or the price at the beginning of the
//                     year. Investors may also track a stock's performance
//                     relative to a benchmark index, such as the S&P 500 or the
//                     Dow Jones Industrial Average.
//                   </p>
//                   <p>
//                     Ticker performance can be influenced by a variety of
//                     factors, including company earnings, economic indicators,
//                     market trends, and global events. By analyzing ticker
//                     performance, investors can gain insights into market trends
//                     and make more informed decisions about buying, selling, or
//                     holding stocks in their portfolio.
//                   </p>
//                   <p>
//                     Market trade history can be used by investors to analyze the
//                     behavior of a particular stock or security over time, to
//                     identify trends and patterns, and to make informed decisions
//                     about buying or selling shares. For example, by studying the
//                     market trade history, investors can identify the level of
//                     demand for a particular stock and use that information to
//                     determine whether to buy or sell.
//                   </p>
//                 </div>
//               </Popup>

//               <div className="">
//                 <Plot data={[trace1, trace2]} layout={layout} />
//               </div>

//               {/* <h1>Stock Detail:</h1> */}
//               {/* <table className="table">
//                 <thead>
//                   <tr>
//                     <th>{sendticker}</th>
//                     <th>Date</th>
//                     <th>Price</th>
//                     <th>High</th>
//                     <th>Low</th>
//                   </tr>
//                 </thead>
//                 <tbody>
//                 {finalList.map((event) => (
//                   <tr key={event}>
//                     <td>{event}</td>
//                     <td>{event}</td>
//                     <td>{event}</td>
//                     <td>{event}</td>
//                     <td>{event}</td>
//                   </tr>
//                 ))}
//                 </tbody>
//               </table> */}
//             </div>
//           </div>
//         </div>
//       </div>
//     </>
//   );
// }

// export default Chart;
