import { React, useState, useEffect } from "react";
import {
  useCreateAssetsMutation,
  useUpdateAssetsMutation,
} from "../store/assetsAPI";
import { useAddTradeMutation } from "../store/tradesAPI";
import Popup from "reactjs-popup";
import { ToastContainer, toast } from "react-toastify";

function BuySellWindow({
  search_ticker,
  price,
  user_id,
  user_assets,
  update_asset,
  onClose,
}) {
  /* eslint-disable no-unused-vars */
  const users_id = user_id;
  const [ticker_S, setSellTicker] = useState("");
  const sellTickerChange = (event) => {
    const value = event.target.value;
    setSellTicker(value);
  };
  const [quantity_S, setSellQuantity] = useState("");
  const sellQuantityChange = (event) => {
    const value = event.target.value;
    setSellQuantity(value);
  };
  const [price_S, setSellPrice] = useState("");
  const sellPriceChange = (event) => {
    const value = event.target.value;
    setSellPrice(value);
  };
  const [ticker_B, setBuyTicker] = useState("");
  const buyTickerChange = (event) => {
    const value = event.target.value;
    setBuyTicker(value);
  };
  const [quantity_B, setBuyQuantity] = useState("");
  const buyQuantityChange = (event) => {
    const value = event.target.value;
    setBuyQuantity(value);
  };
  const [price_B, setBuyPrice] = useState("");
  const buyPriceChange = (event) => {
    const value = event.target.value;
    setBuyPrice(value);
  };
  const [old_C_B, setOldCB] = useState("");
  const [Old_Q, setOldQ] = useState("");

  const [Buy] = useCreateAssetsMutation();
  const [update] = useUpdateAssetsMutation();
  const [addTrade] = useAddTradeMutation();

  const today = new Date();
  const date = today.getDate();
  var month = today.getMonth();
  if (month < 10) {
    month = `0${month}`;
  }
  const year = today.getFullYear();
  const purchase_date = `${year}-${month}-${date}`;
  // const users_assets = user_assets;

  useEffect(() => {
    setBuyTicker(search_ticker);
    setSellTicker(search_ticker);
    setBuyPrice(price);
    setSellPrice(price);
    if (update_asset.length > 0) {
      setOldCB(update_asset[0].cost_basis);
      setOldQ(update_asset[0].amount);
    }
  }, [search_ticker, price, update_asset]);

  const buy_info = {
    ticker: ticker_B,
    amount: quantity_B,
    purchase_price: "",
    sold_price: "",
    purchase_date: purchase_date,
    cost_basis: price_B,
    users_id: users_id,
  };

  const old_CB = old_C_B;

  const old_Q = Old_Q;

  const oldPurPrice = old_CB * old_Q;
  const update_B_Q = parseInt(quantity_B) + parseInt(old_Q);
  // eslint-disable-next-line no-use-before-define
  const update_B_CB = oldPurPrice / update_B_Q;
  const update_B = {
    ticker: ticker_B,
    amount: update_B_Q,
    purchase_price: "",
    sold_price: "",
    purchase_date: purchase_date,
    cost_basis: price_B,
    users_id: users_id,
  };

  const update_S_Q = parseInt(old_Q) - parseInt(quantity_S);
  // const update_S_CB = oldPurPrice / update_S_Q;

  const update_S = {
    ticker: ticker_S,
    amount: update_S_Q,
    purchase_price: "",
    sold_price: "",
    purchase_date: purchase_date,
    cost_basis: price_S,
    users_id: users_id,
  };

  const TR_PUR_PRICE_N = quantity_B * price_B;
  const tradeRecord_B_N = {
    ticker: ticker_B,
    amount: quantity_B,
    purchase_price: TR_PUR_PRICE_N,
    sold_price: "",
    purchase_date: purchase_date,
    cost_basis: price_B,
    users_id: users_id,
  };
  const TR_PUR_PRICE_U = price_B * quantity_B;
  const tradeRecord_Update_B = {
    ticker: ticker_B,
    amount: quantity_B,
    purchase_price: TR_PUR_PRICE_U,
    sold_price: "",
    purchase_date: purchase_date,
    cost_basis: price_B,
    users_id: users_id,
  };

  const TR_PUR_PRICE_S = quantity_S * price_S;
  const tradeRecord_Update_S = {
    ticker: ticker_S,
    amount: quantity_S,
    purchase_price: "",
    sold_price: TR_PUR_PRICE_S,
    purchase_date: purchase_date,
    cost_basis: price_S,
    users_id: users_id,
  };

  const handleBuy = async (event) => {
    event.preventDefault();
    if (update_asset.length === 0) {
      Buy(buy_info);
      addTrade(tradeRecord_B_N);
      toast(`Congrats on your purchase!`);
      setTimeout(function () {
        window.location.reload();
      }, 3000);
    } else if (update_asset.length !== 0) {
      update({ asset_id: update_asset[0].id, info: update_B });
      addTrade(tradeRecord_Update_B);
      toast(`Congrats on your purchase!`);
      setTimeout(function () {
        window.location.reload();
      }, 3000);
    }
  };

  const handleSell = async (event) => {
    event.preventDefault();
    if (update_asset.length !== 0) {
      update({ asset_id: update_asset[0].id, info: update_S });
      addTrade(tradeRecord_Update_S);
      toast(`Make that money!`);
      setTimeout(function () {
        window.location.reload();
      }, 3000);
    } else {
      toast(`Sorry, you can't sell something you dont own.`);
      setTimeout(function () {
        window.location.reload();
      }, 3000);
    }
  };

  useEffect(() => {}, []);

  return (
    <>
      <div className="d-flex justify-content-center">
        <div className="form-container buysell-form m-3">
          <div className="card-body">
            <div className="edu-container">
              <Popup
                trigger={
                  <div className="edu-btn" onClick={onClose}>
                    ?
                  </div>
                }
                position="right"
              >
                <div>
                  <p>
                    Buying shares of stock means purchasing ownership in a
                    specific company. When you buy a share of stock, you are
                    buying a small piece of the company and becoming a
                    shareholder. As a shareholder, you are entitled to a portion
                    of the company's earnings and assets.
                  </p>
                  <p>
                    Shares of stock are typically bought and sold on stock
                    exchanges, such as the New York Stock Exchange or NASDAQ.
                    When you buy a share of stock, you typically pay the current
                    market price, which can be influenced by supply and demand,
                    company performance, and economic factors.
                  </p>
                  <p>
                    Buying shares of stock can be done through a brokerage
                    account, which allows you to place orders to buy or sell
                    shares through an online trading platform. Some companies
                    also offer direct stock purchase plans that allow investors
                    to buy shares directly from the company.
                  </p>
                  <p>
                    Investors buy shares of stock with the hope that the stock
                    price will increase over time, allowing them to sell the
                    shares for a profit. However, stock prices can also
                    decrease, resulting in a loss for investors. It's important
                    to research companies and their financial performance before
                    buying shares of their stock and to diversify your
                    investments to minimize risk.
                  </p>
                </div>
              </Popup>
            </div>
            <form>
              <div className="mb-3">
                <h1 className="form-label-title">Buy:</h1>

                <input
                  onChange={buyTickerChange}
                  value={ticker_B}
                  placeholder="Ticker"
                  required
                  type="text"
                  name="ticker_B"
                  id="ticker_B"
                  className="form-control"
                />
                <label className="form-label" htmlFor="ticker_B">
                  Ticker
                </label>
              </div>
              <div className="mb-3">
                <input
                  onChange={buyPriceChange}
                  value={price_B}
                  placeholder="Price"
                  required
                  type="number"
                  name="price_B"
                  id="price_B"
                  className="form-control"
                />
                <label className="form-label" htmlFor="price_B">
                  Price
                </label>
              </div>
              <div className="mb-3">
                <input
                  onChange={buyQuantityChange}
                  placeholder="Amount"
                  required
                  type="number"
                  name="quantity_B"
                  id="quantity_B"
                  className="form-control"
                />
                <label className="form-label" htmlFor="quantity_B">
                  Amount
                </label>
              </div>
              <button className="site-btns" onClick={handleBuy}>
                Buy
              </button>
            </form>
          </div>
        </div>
        <div className="form-container buysell-form m-3">
          <div className="card-body">
            <form>
              <div className="mb-3">
                <div className="edu-container">
                  <Popup
                    trigger={
                      <div className="edu-btn" onClick={onClose}>
                        ?
                      </div>
                    }
                    position="left"
                  >
                    <div>
                      Selling stock means to dispose of the ownership in a
                      particular company that an investor has held. When an
                      investor sells stock, they are giving up their ownership
                      stake in the company, and in return, they receive a price
                      determined by the current market value of the shares.
                      <p>
                        Selling stock can be done on stock exchanges through a
                        brokerage account, either through an online trading
                        platform or by placing orders through a broker.
                        Investors can sell all or part of their holdings,
                        depending on their investment goals and financial needs.
                      </p>
                      <p>
                        Selling stock can be done for a variety of reasons,
                        including taking profits on an investment, reducing
                        exposure to a particular stock or industry, or raising
                        cash for other investments or expenses. It's important
                        to note that selling stock may result in capital gains
                        or losses, depending on the sale price compared to the
                        purchase price.
                      </p>
                      <p>
                        Investors who sell stock are subject to capital gains
                        taxes on any profits they earn from the sale. However,
                        they can also use losses from the sale of stock to
                        offset gains from other investments, reducing their
                        overall tax liability.
                      </p>
                    </div>
                  </Popup>
                </div>
                <h1 className="form-label-title">Sell:</h1>
                <input
                  onChange={sellTickerChange}
                  value={ticker_S}
                  placeholder="Ticker"
                  required
                  type="text"
                  name="ticker_S"
                  id="ticker_S"
                  className="form-control"
                />
                <label className="form-label" htmlFor="ticker_S">
                  Ticker
                </label>
              </div>
              <div className="mb-3">
                <input
                  onChange={sellPriceChange}
                  value={price_S}
                  placeholder="Price"
                  required
                  type="number"
                  name="price_S"
                  id="price_S"
                  className="form-control"
                />
                <label className="form-label" htmlFor="price_S">
                  Price
                </label>
              </div>
              <div className="mb-3">
                <input
                  onChange={sellQuantityChange}
                  value={quantity_S}
                  placeholder="Amount"
                  required
                  type="number"
                  name="quantity_S"
                  id="quantity_S"
                  className="form-control"
                />
                <label className="form-label" htmlFor="quantity_S">
                  Amount
                </label>
              </div>
              <button className="site-btns" onClick={handleSell}>
                Sell
              </button>
            </form>
          </div>
        </div>
      </div>

      <ToastContainer
        position="bottom-right"
        autoClose={2000}
        hideProgressBar={false}
        newestOnTop
        closeOnClick
        rtl={false}
        pauseOnFocusLoss={false}
        draggable
        pauseOnHover={false}
        theme="dark"
        z-index="999999"
      />
    </>
  );
}

export default BuySellWindow;
