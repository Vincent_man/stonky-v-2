import { React, useState, useRef } from "react";
// import Chart from "./ChartingComponet";
import BuySellWindow from "./BuySellAssets";
import TradeHistory from "./TradeHistory";
// import LeaderBoard from "./LeaderBoard";
import { useGetTokenQuery } from "../store/authAPI";
import { useGetAssetsQuery } from "../store/assetsAPI";
import { useGetTradeQuery } from "../store/tradesAPI";
import background from "../images/fullpurpgreen.jpg";
import stonkguy from "../images/stonkguy.png";
import savings from "../images/savings.png";
import purpstock from "../images/purpstock.png";
import Chart from "./TestChart";
import { Parallax, ParallaxLayer } from "@react-spring/parallax";

// import Popup from "reactjs-popup";

function TradingPage() {
  const parallax = useRef(null);
let users_id = undefined;
const { data, isLoading } = useGetTokenQuery();
if (!isLoading) {
  users_id = data.account.id;
}

const key = "7vDQqQzxpgFK35uMCyKnClz4BsBnert5";
const [Ticker, setTicker] = useState("");
const handleTickerChange = (event) => {
  const value = event.target.value;
  setTicker(value);
};
const sendticker = Ticker.toUpperCase();
const [count, setQueryCount] = useState("");
const [stockData, setStockData] = useState([]);
const x = [];
const y = [];
const low = [];
const high = [];
const clse = [];
const StockDetail = [];
const users_assets = [];
const tickers = [];
let Price = 0;
const tradeHistory = [];
const buyEvents = [];
const sellEvents = [];
const finalList = [];

const GetTrades = () => {
  var a = useGetTradeQuery();
  var as = a.data;
  if (as !== undefined) {
    for (let a of as) {
      // if (a.users_id === users_id && a.sold_price === ""){
      if (a.sold_price === "") {
        tradeHistory.push(a);
        buyEvents.push(a);
      } else if (a.purchase_price === "") {
        tradeHistory.push(a);
        sellEvents.push(a);
      }
    }
  }
};
GetTrades();

const GetAssets = () => {
  var a = useGetAssetsQuery();
  var as = a.data;
  if (as !== undefined) {
    for (let a of as) {
      if (a.users_id === users_id) {
        users_assets.push(a);
        tickers.push([a.ticker, { id: a }]);
      }
    }
  }
};
GetAssets();

// const OneMonthToToday = () => {
//   const today = new Date();
//   const year = today.getFullYear();
//   const month = String(today.getMonth() + 1).padStart(2, "0");
//   const day = String(today.getDate()).padStart(2, "0");
//   const formattedToday = `${year}-${month}-${day}`;

//   const oneMonthAgo = new Date();
//   oneMonthAgo.setMonth(oneMonthAgo.getMonth() - 1);

//   const year1 = oneMonthAgo.getFullYear();
//   const month1 = String(oneMonthAgo.getMonth() + 1).padStart(2, "0");
//   const day1 = String(oneMonthAgo.getDate()).padStart(2, "0");
//   const formattedOneMonthAgo = `${year1}-${month1}-${day1}`;

//   return [formattedToday, formattedOneMonthAgo];
// };

const OneYearToToday = () => {
  const today = new Date();
  const year = today.getFullYear();
  const month = String(today.getMonth() + 1).padStart(2, "0");
  const day = String(today.getDate()).padStart(2, "0");
  const formattedToday = `${year}-${month}-${day}`;

  const oneYearAgo = new Date();
  oneYearAgo.setFullYear(oneYearAgo.getFullYear() - 1);

  const year1 = oneYearAgo.getFullYear();
  const month1 = String(oneYearAgo.getMonth() + 1).padStart(2, "0");
  const day1 = String(oneYearAgo.getDate()).padStart(2, "0");
  const formattedOneYearAgo = `${year1}-${month1}-${day1}`;

  return [formattedToday, formattedOneYearAgo];
};

// stretch goal for fancy chart
function getDateRange(startDate, endDate) {
  var dates = [];
  var currentDate = new Date(startDate);
  endDate = new Date(endDate);

  while (currentDate <= endDate) {
    dates.push(new Date(currentDate).toISOString().slice(0, 10));
    currentDate.setDate(currentDate.getDate() + 1);
  }
  return dates;
}

const [Today, OneYrAgo] = OneYearToToday();
let TodaysDate = Today;
let OneYrAgoDate = OneYrAgo;
const dates = getDateRange(OneYrAgoDate, TodaysDate);
let Dates = dates;
const fetchData = async () => {
  const StockDataUrl = `https://api.polygon.io/v2/aggs/ticker/${sendticker}/range/1/day/${OneYrAgo}/${Today}?adjusted=true&sort=asc&limit=600&apiKey=${key}`;
  const response = await fetch(StockDataUrl);
  if (response.ok) {
    const data = await response.json();
    setTicker(data.ticker);
    setStockData(data.results);
    setQueryCount(data.queryCount);
  }
};

const SetChart = async () => {
  var query_count = parseInt(count);
  for (var i = 1; i <= query_count; i++) {
    x.push(i);
  }
  if (stockData !== undefined) {
    stockData.forEach((data) => {
      y.push(data.h);
      low.push(data.l);
      high.push(data.h);
      clse.push(data.c);
      StockDetail.push(data.h);
    });
  }
  var date = dates;
  var clos = clse;
  var High = high;
  var Low = low;

  for (let i = 0; i < dates.length; i++) {
    finalList.push(date[i]);
    finalList.push(clos[i]);
    finalList.push(High[i]);
    finalList.push(Low[i]);
  }
  const p = StockDetail.length;
  Price = StockDetail[p - 1];
};

const handleSearch = async (e) => {
  await fetchData();
};
SetChart();

var asset_to_update = [];
const checkrrr = (da_tic) => {
  for (let i of users_assets) {
    if (i.ticker === da_tic) {
      asset_to_update.push(i);
    } else {
    }
  }
};
checkrrr(sendticker);


  if (users_id !== undefined) {
    return (
      <>
        <div style={{ width: "100%", height: "100%", background: "#000000" }}>
          <Parallax ref={parallax} pages={3}>
            <ParallaxLayer
              offset={1}
              speed={1}
              style={{ backgroundColor: "#000000" }}
            />
            <ParallaxLayer
              offset={2}
              speed={1}
              style={{ backgroundColor: "#000000" }}
            />

            <ParallaxLayer
              offset={0}
              speed={0}
              factor={3}
              style={{
                backgroundImage: `url(${background})`,
                backgroundSize: "cover",
                backgroundPosition: "center",
              }}
            />

            <ParallaxLayer
              offset={1.3}
              speed={-0.3}
              style={{ pointerEvents: "none", zIndex: "2", opacity: 0.7 }}
            >
              <img
                alt="savings"
                src={savings}
                style={{ width: "15%", marginLeft: "70%" }}
              />
            </ParallaxLayer>

            <ParallaxLayer offset={1} speed={0.8} style={{ opacity: 0.6 }}>
              {/* <img
              alt="purpstock"
              src={purpstock}
              style={{
                display: "block",
                width: "20%",
                marginLeft: "55%",
              }}
            /> */}
              <img
                alt="savings"
                src={savings}
                style={{
                  display: "block",
                  width: "10%",
                  marginLeft: "15%",
                  zIndex: "5",
                }}
              />
            </ParallaxLayer>

            <ParallaxLayer offset={1.75} speed={0.5} style={{ opacity: 0.1 }}>
              <img
                alt="purpstock"
                src={purpstock}
                style={{ display: "block", width: "20%", marginLeft: "70%" }}
              />
              <img
                alt="purpstock"
                src={purpstock}
                style={{ display: "block", width: "20%", marginLeft: "40%" }}
              />
            </ParallaxLayer>

            <ParallaxLayer offset={1} speed={0.2} style={{ opacity: 0.2 }}>
              <img
                alt="purpstock"
                src={purpstock}
                style={{ display: "block", width: "10%", marginLeft: "10%" }}
              />
              <img
                alt="purpstock"
                src={purpstock}
                style={{ display: "block", width: "20%", marginLeft: "75%" }}
              />
            </ParallaxLayer>

            <ParallaxLayer offset={1.6} speed={-0.1} style={{ opacity: 0.4 }}>
              <img
                alt="purpstock"
                src={purpstock}
                style={{ display: "block", width: "20%", marginLeft: "60%" }}
              />
              <img
                alt="purpstock"
                src={purpstock}
                style={{
                  display: "block",
                  width: "25%",
                  marginLeft: "30%",
                }}
              />
              <img
                alt="purpstock"
                src={purpstock}
                style={{ display: "block", width: "10%", marginLeft: "80%" }}
              />
            </ParallaxLayer>

            <ParallaxLayer
              offset={2.6}
              speed={0.4}
              style={{ opacity: 0.9, zIndex: "2" }}
            >
              <img
                alt="purpstock"
                src={purpstock}
                style={{
                  display: "block",
                  width: "20%",
                  marginLeft: "5%",
                }}
              />
              <img
                alt="purpstock"
                src={purpstock}
                style={{ display: "block", width: "15%", marginLeft: "75%" }}
              />
            </ParallaxLayer>

            {/* <ParallaxLayer
              offset={2.5}
              speed={-0.4}
              style={{
                display: "flex",
                alignItems: "center",
                justifyContent: "center",
                pointerEvents: "none",
              }}
            >
              <img alt="trading" src={trading} style={{ width: "60%" }} />
            </ParallaxLayer> */}

            <ParallaxLayer
              offset={0.2}
              speed={-1}
              style={{
                backgroundSize: "32%",
                backgroundPosition: "left",
                backgroundImage: `url(${stonkguy})`,
                opacity: 0.6,
                zIndex: "1",
              }}
            />

            <ParallaxLayer
              offset={0}
              speed={0.1}
              onClick={() => parallax.current.scrollTo(0)}
              style={{
                justifyContent: "center",
                zIndex: "10",
                pointerEvents: "visible",
              }}
            >
              <div>
                <input
                  onChange={handleTickerChange}
                  value={Ticker}
                  type="text"
                  className="form-control charting-search"
                  placeholder="Search for Ticker (XXXX)"
                  aria-label="Search for data"
                  aria-describedby="button-addon2"
                  maxLength={4}
                />
                <button
                  onClick={handleSearch}
                  className="charting-btns"
                  id="button-addon2"
                >
                  Search for a Ticker
                </button>
              </div>

              <div style={{ pointerEvents: "auto" }}>
                <Chart
                  y={y}
                  x={x}
                  high={high}
                  low={low}
                  ticker={sendticker}
                  Today={TodaysDate}
                  OneYearAgo={OneYrAgoDate}
                  Dates={Dates}
                />
              </div>
            </ParallaxLayer>

            <ParallaxLayer
              offset={1}
              speed={0.1}
              onClick={() => parallax.current.scrollTo(1)}
              style={{
                display: "flex",
                alignItems: "center",
                justifyContent: "center",
                zIndex: "10",
              }}
            >
              <div className="container-fluid">
                <BuySellWindow
                  search_ticker={sendticker}
                  price={Price}
                  user_id={users_id}
                  user_assets={users_assets}
                  update_asset={asset_to_update}
                />
              </div>
            </ParallaxLayer>

            <ParallaxLayer
              offset={2}
              speed={-0}
              style={{
                display: "flex",
                alignItems: "center",
                justifyContent: "center",
                zIndex: "10",
              }}
              onClick={() => parallax.current.scrollTo(0)}
            >
              <TradeHistory
                buyEvents={buyEvents}
                sellEvents={sellEvents}
                tradeHistory={tradeHistory}
              />
            </ParallaxLayer>

            {/* <FullpageSection
                style={{
                  ...SectionStyle,
                  backgroundImage: `url(${leader})`,
                  backgroundSize: "cover",
                }}
              >
                <div className="container-fluid">
                  <LeaderBoard />
                </div>
              </FullpageSection> */}
            {/* <div>
              <h1>some more stuff in here</h1>
            </div> */}
          </Parallax>
        </div>
      </>
    );
  }
}

export default TradingPage;
