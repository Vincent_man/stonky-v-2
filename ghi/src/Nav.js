import { useGetTokenQuery, useLogoutUserMutation } from "./store/authAPI";
import { useNavigate } from "react-router-dom";
import React, { useState } from "react";
// import "./Site.css";
import "./test.css";
import Login from "./Users/Login";
import SignupForm from "./Users/SignUp";
import stonky_nav from "./images/BlackWebsite.png";
import { ToastContainer, toast } from "react-toastify";

function Nav() {
  const { data, isLoading } = useGetTokenQuery();
  const [logout] = useLogoutUserMutation();
  const navigate = useNavigate();
  const [showModal, setShowModal] = useState(false);
  const [showModalLogin, setShowModalLogin] = useState(false);

  if (isLoading) {
    return <progress className="progress is-primary" max="100" />;
  }

  function openForm() {
    setShowModal(true);
  }

  function closeForm() {
    setShowModal(false);
  }

  function openFormLogin() {
    setShowModalLogin(true);
  }

  function closeFormLogin() {
    setShowModalLogin(false);
  }

  const handleSubmit = async (e) => {
    e.preventDefault();
    await logout();
    toast(`See you next time!`);
    navigate("/");
    setTimeout(function () {
      window.location.reload();
    }, 3000);
  };

  return (
    <>
      <div className="topnav">
        <nav className="">
            <button className="" onClick={() => window.location.href = '/'}>
              <img
                src={stonky_nav}
                style={{ width: "250px" }}
                height="40"
                alt=""
              />
            </button>
          <div className="topnav-right">
            <ul className="btn-container">
              {data !== null ? (
                <>
                  <p className="topnav-right">
                    {/* <a href="#">Hello, {data.account.name}!</a> */}
                    <span>Hello, {data.account.name}!</span>
                  </p>
                  <div className="nav-btns topnav-right">
                    <button className="" href="#" onClick={handleSubmit}>
                      Logout
                    </button>
                  </div>
                  <div className="nav-btns topnav-right">
                    <button data-bs-toggle="dropdown" aria-expanded="false">
                      Explore
                    </button>
                    <ul className="dropdown-menu dropdown-btn">
                      <li>
                        <button
                          className="dropdown-item nav-btns dropdown-btn"
                          onClick={() => navigate("/")}
                        >
                          Home
                        </button>
                      </li>
                      <li>
                        <button
                          className="dropdown-item nav-btns dropdown-btn"
                          onClick={() => navigate("/profilepage")}
                        >
                          Profile
                        </button>
                      </li>
                      {/* <li>
                        <button
                          className="dropdown-item nav-btns dropdown-btn"
                          onClick={() => navigate("/portfoliopage")}
                        >
                          Porfolio
                        </button>
                      </li> */}
                      <li>
                        <button
                          className="dropdown-item nav-btns dropdown-btn"
                          onClick={() => navigate("/trading")}
                        >
                          Trading
                        </button>
                      </li>
                    </ul>
                  </div>
                </>
              ) : (
                <>
                  <div className="topnav-right">Hello, Welcome to StoNky!</div>

                  <div className="nav-btns topnav-right">
                    <button data-bs-toggle="dropdown" aria-expanded="false">
                      SignUp/Login!
                    </button>
                    <ul className="dropdown-menu dropdown-btn">
                      <li>
                        <button
                          className="dropdown-item nav-btns dropdown-btn"
                          onClick={openForm}
                        >
                          SignUp
                        </button>
                      </li>
                      <li>
                        <button
                          className="dropdown-item nav-btns dropdown-btn"
                          onClick={openFormLogin}
                        >
                          Login
                        </button>
                      </li>
                    </ul>
                  </div>
                  {showModal ? (
                    <div className="modal-backdrop">
                      <div className="modal-content">
                        <SignupForm onClose={closeForm} />
                      </div>
                    </div>
                  ) : showModalLogin ? (
                    <div className="modal-backdrop">
                      <div className="modal-content">
                        <Login onClose={closeFormLogin} />
                      </div>
                    </div>
                  ) : null}
                </>
              )}
            </ul>
          </div>
        </nav>
      </div>
      <ToastContainer
        position="bottom-right"
        autoClose={2000}
        hideProgressBar={false}
        newestOnTop
        closeOnClick
        rtl={false}
        pauseOnFocusLoss={false}
        draggable
        pauseOnHover={false}
        theme="dark"
        z-index="999999"
      />
    </>
  );
}

export default Nav;
