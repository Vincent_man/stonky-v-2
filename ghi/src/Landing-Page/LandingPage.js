import "./LandingPage.css";
import React from "react";
// import Popup from "reactjs-popup";
import HeroContainer from "./HeroContainer";
import Timeline from "./Timeline";
// import purpspin from "../images/purpspin.png";
// import purpbrain from "../images/purpspin.png";
// import purpstrat from "../images/purpstrat.png";
import Fullpage, {
  FullPageSections,
  FullpageSection,
  FullpageNavigation,
} from "@ap.cx/react-fullpage";
// import cards from "../images/fullpurpgreen.jpg";
import hero from "../images/mostlygreen.jpg";
// import ani from "../images/smallleft.jpg";



const SectionStyle = {
  height: "100vh",
  width: "100%",
  display: "flex",
  justifyContent: "center",
  alignItems: "center",
};

function LandingPage() {

  return (
    <>
      <Fullpage>
        <FullpageNavigation />
        <FullPageSections>
          <div className="landing-page">
            <FullpageSection
              style={{
                ...SectionStyle,
                backgroundImage: `url(${hero})`,
                backgroundSize: "cover",
                opacity: 4,
              }}
            >
              <div>
                <HeroContainer className="hero-container" />
              </div>
            </FullpageSection>

            {/* <FullpageSection
              style={{
                ...SectionStyle,
                backgroundImage: `url(${cards})`,
                backgroundSize: "cover",
                opacity: 4,
              }}
            >
              <div className="profile-col">
                <Popup
                  trigger={
                    <button>
                      <div className="profile-colcard">
                        <img alt="" src={purpspin} className="profile-img" />
                        <div className="profilecard-body">
                          <h5 className="profilecard-title">Beginner</h5>
                          <h6> Click for more info!</h6>
                        </div>
                      </div>
                    </button>
                  }
                  position="bottom"
                >
                  <div>
                    New to the world of buying and selling stocks, and you have
                    little or no experience with how the stock market works. You
                    may not be familiar with the terminology, strategies, and
                    risks associated with investing in stocks.
                  </div>
                </Popup>
                <Popup
                  trigger={
                    <button>
                      <div className="profile-colcard">
                        <img alt="" src={purpbrain} className="profile-img" />
                        <div className="profilecard-body">
                          <h5 className="profilecard-title">Intermediate</h5>
                          <h6> Click for more info!</h6>
                        </div>
                      </div>
                    </button>
                  }
                  position="top"
                >
                  <div>
                    You have some experience and knowledge in buying and selling
                    stocks. You are no longer a beginner, but you are not yet an
                    expert. As an intermediate trader, you have a basic
                    understanding of how the stock market works, the different
                    types of stocks, and the strategies used to make profitable
                    trades.
                  </div>
                </Popup>
                <Popup
                  trigger={
                    <button>
                      <div className="profile-colcard">
                        <img alt="" src={purpstrat} className="profile-img" />
                        <div className="profilecard-body">
                          <h5 className="profilecard-title">Advanced</h5>
                          <h6> Click for more info!</h6>
                        </div>
                      </div>
                    </button>
                  }
                  position="bottom"
                >
                  <div>
                    You have a deep understanding of the stock market, including
                    the different sectors, companies, and market trends. You may
                    have developed your own unique trading strategy and may use
                    advanced technical and fundamental analysis techniques to
                    make profitable trades. You are likely skilled in using
                    different trading tools and platforms to execute trades
                    efficiently.
                  </div>
                </Popup>
              </div>
            </FullpageSection> */}

            <FullpageSection
              style={{
                ...SectionStyle,
                background: "black",
                backgroundSize: "cover",
                opacity: 4,
              }}
            ><div>
              < Timeline />
            </div>
            </FullpageSection>
          </div>
        </FullPageSections>
      </Fullpage>
    </>
  );
}

export default LandingPage;
