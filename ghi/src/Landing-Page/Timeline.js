import "../timeline.scss";
import React from "react";
import adrian from "../images/mostlygreen.jpg"
import vincent from "../images/purp2.jpg";
import alissa from "../images/purp.jpeg";
import ash from "../images/green2.jpeg"
import adrianpic from "../images/adrian.png";
import vincentpic from "../images/vincent.png";
import alissapic from "../images/alissa.png";
import ashpic from "../images/ash.png";

function Timeline() {

      return (
        <div>
          <section id="timeline">
            <div className="tl-item">
              <div
                className="tl-bg"
                style={{
                  backgroundImage: `url(${adrian})`,
                }}
              ></div>

              <div className="tl-year">
                <p className="f2 heading--sanSerif">ADRIAN MA</p>
              </div>

              <div className="tl-content">
                <h1>
                  <a href="https://www.linkedin.com/in/adriancyma/">
                    linkedin.com/in/adriancyma
                  </a>
                </h1>
                <p>
                  <img alt="" src={adrianpic} className="profile-img" />
                </p>
                {/* <p>
                  Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed
                  do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                  Ut enim ad minim veniam, quis nostrud exercitation ullamco
                  laboris nisi ut aliquip ex ea commodo consequat. Duis aute
                  irure dolor in reprehenderit in voluptate velit.
                </p> */}
              </div>
            </div>

            <div className="tl-item">
              <div
                className="tl-bg"
                style={{
                  backgroundImage: `url(${alissa})`,
                }}
              ></div>

              <div className="tl-year">
                <p className="f2 heading--sanSerif">ALISSA RENTERIA</p>
              </div>

              <div className="tl-content">
                <h1 className="f3 text--accent ttu">
                  {" "}
                  <a href="                  https://www.linkedin.com/in/alissa-renteria/">
                    linkedin.com/in/alissa-renteria
                  </a>
                </h1>
                <p>
                  <img alt="" src={alissapic} className="profile-img" />
                </p>
                {/* <p>
                  Suspendisse potenti. Sed sollicitudin eros lorem, eget
                  accumsan risus dictum id. Maecenas dignissim ipsum vel mi
                  rutrum egestas. Donec mauris nibh, facilisis ut hendrerit vel,
                  fringilla sed felis. Morbi sed nisl et arcu.
                </p> */}
              </div>
            </div>

            <div className="tl-item">
              <div
                className="tl-bg"
                style={{
                  backgroundImage: `url(${ash})`,
                }}
              ></div>

              <div className="tl-year">
                <p className="f2 heading--sanSerif">ASH AVILA</p>
              </div>

              <div className="tl-content">
                <h1 className="f3 text--accent ttu">
                  {" "}
                  <a href="                  https://www.linkedin.com/in/ashavila/">
                    linkedin.com/in/ashavila
                  </a>
                </h1>
                <p>
                  <img alt="" src={ashpic} className="profile-img" />
                </p>
                {/* <p>
                  Mauris cursus magna at libero lobortis tempor. Suspendisse
                  potenti. Aliquam interdum vulputate neque sit amet varius.
                  Maecenas ac pulvinar nisi. Fusce vitae nunc ultrices,
                  tristique dolor at, porttitor dolor. Etiam id cursus arcu, in
                  dapibus nibh. Pellentesque non porta leo. Nulla eros odio,
                  egestas quis efficitur vel, pretium sed.
                </p> */}
              </div>
            </div>

            <div className="tl-item">
              <div
                className="tl-bg"
                style={{
                  backgroundImage: `url(${vincent})`,
                }}
              ></div>

              <div className="tl-year">
                <p className="f2 heading--sanSerif">VINCENT MANFRE</p>
              </div>

              <div className="tl-content">
                <h1 className="f3 text--accent ttu">
                  <a href="https://www.linkedin.com/in/vincent-manfre/">
                    linkedin.com/in/vincent-manfre
                  </a>
                </h1>
                <p>
                  <img alt="" src={vincentpic} className="profile-img" />
                </p>
                {/* <p>
                  Suspendisse ac mi at dolor sodales faucibus. Nunc sagittis
                  ornare purus non euismod. Donec vestibulum efficitur tortor,
                  eget efficitur enim facilisis consequat. Vivamus laoreet
                  laoreet elit. Ut ut varius metus, bibendum imperdiet ex.
                  Curabitur diam quam, blandit at risus nec, pulvinar porttitor
                  lorem. Pellentesque dolor elit.
                </p> */}
              </div>
            </div>
          </section>
        </div>
      );
}

export default Timeline;
