import React, { useState } from "react";
import SignupForm from "../Users/SignUp";


function HeroContainer() {
  const [showModal, setShowModal] = useState(false);

  function openForm() {
    setShowModal(true);
  }

  function closeForm() {
    setShowModal(false);
  }

  return (
    <>
      <div className="hero-container">
        <h1>Welcome to StoNky</h1>
        <h2>Where we make stock trading fun & a little competitive...</h2>
        {showModal && (
          <div className="modal-backdrop">
            <div className="modal-content">
              <SignupForm onClose={closeForm} />
            </div>
          </div>
        )}
        <button className="hero-btns" onClick={openForm}>
          Create Account
        </button>
      </div>


    </>
  );
}

export default HeroContainer;
