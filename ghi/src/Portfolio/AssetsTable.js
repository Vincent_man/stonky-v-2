import {React, useEffect, useState } from "react";
import { useGetTokenQuery } from "../store/authAPI";
import { useGetAssetsQuery } from "../store/assetsAPI";


function AssetTable() {
  let users_id = undefined
  const {data, isLoading} = useGetTokenQuery();
  if (!isLoading){
    users_id = data.account.id
  }
  const users_assets = []
  const GetAssets= () => {
    var a = useGetAssetsQuery();
    var as = a.data
    if (as !== undefined){
      for (let a of as){
        if (a.users_id === users_id){
          users_assets.push(a)
        }
      }
    }
  }
  GetAssets();
//   useEffect(() => {

//   }, []);


  if (users_id !== undefined){

  return (
    <>
      <div
        className="container-fluid"
        style={{ border: "1px solid black", backgroundColor: "pink" }}
      >
        <h1>Your Assets</h1>
        <div className="row" align="center">
          <div className="col-lg-4 mx-auto">
            <table className="table table-striped">
              <thead>
                <tr>
                  <th>Ticker</th>
                  <th>Shares</th>
                  <th>Price</th>
                  <th>Date</th>
                  <th>Total</th>
                </tr>
              </thead>
              <tbody>
                {users_assets.map((event) => (
                  <tr key={event.id}>
                    <td>{event.ticker}</td>
                    <td>{event.amount}</td>
                    <td>{event.cost_basis}</td>
                    <td>{event.purchase_date}</td>
                    <td>{event.cost_basis}</td>
                  </tr>
                ))}
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </>
    )};
    }

export default AssetTable;
