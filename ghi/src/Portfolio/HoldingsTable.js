import React from 'react';

function HoldingsTable({ holdings }) {
  return (
    <table className="holdings-table">
      <thead>
        <tr>
          <th>Symbol</th>
          <th>Shares</th>
          <th>Price</th>
          <th>Market Value</th>
        </tr>
      </thead>
      <tbody>
        {holdings.map((holding) => (
          <tr key={holding.symbol}>
            <td>{holding.symbol}</td>
            <td>{holding.shares}</td>
            <td>{holding.price}</td>
            <td>{holding.marketValue}</td>
          </tr>
        ))}
      </tbody>
    </table>
  );
}

export default HoldingsTable;