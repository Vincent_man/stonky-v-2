// import React, { useState } from 'react';
// import HoldingsTable from './HoldingsTable';
// import TradeHistoryTable from './TradeHistoryTable';

// function PortfolioPage() {

//   const [holdings, setHoldings] = useState([
//     { ticker: 'AAPL', quantity: 100, avgPrice: 135.50 },
//     { ticker: 'TSLA', quantity: 50, avgPrice: 700.00 },
//     { ticker: 'MSFT', quantity: 75, avgPrice: 250.00 },
//   ]);

//   const [tradeHistory, setTradeHistory] = useState([
//     { ticker: 'AAPL', action: 'Buy', quantity: 50, price: 137.00, date: '2021-07-13' },
//     { ticker: 'TSLA', action: 'Sell', quantity: 25, price: 750.00, date: '2020-08-19' },
//     { ticker: 'MSFT', action: 'Buy', quantity: 50, price: 255.00, date: '2021-09-16' },
//   ]);

//   return (
//     <div className="portfolio-page">
//       <h1>My Trade Portfolio</h1>
//       <HoldingsTable holdings={holdings} />
//       <TradeHistoryTable tradeHistory={tradeHistory} />
//     </div>
//   );
// }

// export default PortfolioPage;