import "../test.css"
import Chart from "../Trading-Page/TestChart";
import BuySell from "./BuySell";
import ToggleSwitch from "./BuySellToggle";

function Skeleton() {



return (
    <>
    <div className="wrapper">
        <div className="main">
            <div className="chart-col">
                <div className="charting-container">
                    <div className="search-container">
                        <div className="search-col">
                            <input type="text" className="search-input" placeholder="Search..." />
                        </div>
                        <div className="whitespace-col">
                            <button className="search-button">Search</button>
                        </div>
                    </div>
                    <div className="asset-info-row">
                        <div className="asset-info-col">
                            <div className="asset-name-row">
                                <p className="asset-info-NAME">xyz</p>
                            </div>
                            <div className="market-price-row">
                                <p className="asset-info-PRICE">$514.38</p>
                            </div>
                            <div className="change-row">
                                <p className="asset-info-VALUE-CHANGE">+5.38%</p>
                            </div>
                        </div>
                    </div>
                    <div className="chart-row">
                        <div className="chart-frame">
                            <Chart/>
                        </div>
                    </div>
                </div>
            </div>
            <div className="order-column">
                <div className="order-form-container">
                    <BuySell/>
                </div>
            </div>
        </div>
        <div className="about-section">
            <div className="about-col">
                <section>
                    <div className="about-heading-row">
                         <p className="about-asset-NAME">About XZY</p>
                    </div>
                    <div className="tags-grid-container">
                        <div className="tags-row-1">
                            <div className="tag-background ">
                                <p>tags  </p>
                            </div>
                            <div className="tag-background ">
                                <p>tags  </p>
                            </div>
                            <div className="tag-background ">
                                <p>tags  </p>
                            </div>
                        </div>
                        <div className="tags-row-2">
                            <div className="tag-background ">
                                <p>tags  </p>
                            </div>
                            <div className="tag-background ">
                                <p>tags  </p>
                            </div>
                            <div className="tag-background ">
                                <p>tags  </p>
                            </div>
                        </div>
                    </div>
                    <div className="about-text-row">
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                            sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                             Ut enim ad minim veniam, quis nostrud exercitation ullamco
                             laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure
                             dolor in.</p>
                    </div>
                </section>
            </div>
            <div className="bottom-right-col">
            </div>
        </div>
        <div>
            {/* <ToggleSwitch/> */}
        </div>
    </div>
    </>
)
}

export default Skeleton
