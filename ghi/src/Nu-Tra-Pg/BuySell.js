import { React, useState, useRef } from "react";
import { useGetTokenQuery } from "../store/authAPI";
import ToggleSwitch from "./BuySellToggle";
import "../test.css"



function BuySell() {
  const [isChecked, setIsChecked] = useState(false);

  const handleToggle = () => {
    setIsChecked(!isChecked);
  };

  const label = isChecked ? 'Buy' : 'Sell';
  const buttonBackgroundColor = isChecked ? "#30CD9A" : "#FA9E9E";
  const formOutline = isChecked ? "#1aff00" : "#f32121"

    return (
        <>

        <div className="card-container">
            <div className="buy-sell-card">
                <div className="name-toggle-row">
                    <div className="asset-name-col">
                        <p className="asset-name-heading">XYZ</p>
                    </div>
                    <div className="buy-sell-toggle-col">
                         <div className="toggle-switch-container">
                            <label htmlFor="toggle" className="toggle-label-text">{label}</label>
                            <div className="toggle-switch">
                                <input
                                type="checkbox"
                                id="toggle"
                                className="toggle-input"
                                checked={isChecked}
                                onChange={handleToggle}
                                />
                                <label htmlFor="toggle" className="toggle-label"></label>
                            </div>
                        </div>
                    </div>
                </div>
                <form className="buy-sell-form-row">
                    <div className="buy-sell-form-shares-row">
                        <div className="buy-sell-form-label">
                            <label>Shares</label>
                        </div>
                        <div className="buy-sell-form-input">
                            <input className="buy-sell-input" placeholder="Amount" />
                        </div>
                    </div>
                    <div className="buy-sell-form-shares-row">
                        <div className="buy-sell-form-label">
                            <label >Market Price</label>
                        </div>
                        <div className="buy-sell-form-input">
                            <input className="buy-sell-input" placeholder="Price" />
                        </div>
                    </div>
                    <div className="buy-sell-form-shares-row">
                        <div className="buy-sell-form-label">
                            <label>Estimated Total</label>
                        </div>
                        <div className="buy-sell-form-input">
                            <input className="buy-sell-input" placeholder="Total" />
                        </div>
                    </div>
                </form>
                <div className="buy-sell-button-row">
                    <div className="buy-sell-button-space-col"></div>
                    <div className="buy-sell-button-col">
                        <button className="buy-sell-button" style={{ backgroundColor: buttonBackgroundColor, transition: 'background-color 0.4s' }}>button here</button>
                    </div>
                    <div className="buy-sell-button-space-col"></div>
                </div>
                <div className="buy-sell-balance-row">
                    <div className="buy-sell-button-space-col"></div>
                    <div className="buy-sell-balance-col">
                        <p>$507,290</p>
                    </div>
                    <div className="buy-sell-button-space-col"></div>
                </div>
            </div>
        </div>
        </>
    )
}

export default BuySell
