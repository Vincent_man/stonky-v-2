import React, { useState } from 'react';

const ToggleSwitch = () => {
  const [isChecked, setIsChecked] = useState(false);

  const handleToggle = () => {
    setIsChecked(!isChecked);
  };

  const label = isChecked ? 'Buy' : 'Sell';
const buttonBackgroundColor = isChecked ? "#1aff00" : "#f32121";

  return (
    <>
    {/* <div className="toggle-switch-container">
      <div className="toggle-switch">
        <input
          type="checkbox"
          id="toggle"
          className="toggle-input"
          checked={isChecked}
          onChange={handleToggle}
        />
      <label htmlFor="toggle" className="toggle-label">
          {label}
      </label>
      </div>
    </div> */}
    <div className="toggle-switch-container">
      <label htmlFor="toggle" className="toggle-label-text">{label}</label>
      <div className="toggle-switch">
        <input
          type="checkbox"
          id="toggle"
          className="toggle-input"
          checked={isChecked}
          onChange={handleToggle}
        />
        <label htmlFor="toggle" className="toggle-label"></label>
      </div>

    </div>
    <button className="buy-sell-button" style={{ backgroundColor: buttonBackgroundColor }}>
  fok
</button>
    </>
  );
};

export default ToggleSwitch;
