import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  users: {},
  token: null,
};

export const usersSlice = createSlice({
  name: "users",
  initialState: initialState,
  reducers: {
    logout: () => initialState,
    setUsers: (state, { payload }) => {
      state.token = payload.access_token;
      state.users = payload.account;
    },
  },
  extraReducers: {},
});






export default usersSlice.reducer;
export const { logout, setUsers } = usersSlice.actions;
