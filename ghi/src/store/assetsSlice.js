import { createSlice } from "@reduxjs/toolkit";

const assetsinitialState = {
  assets: {},
  token: null,
};

export const assetsSlice = createSlice({
  name: "assets",
  initialState: assetsinitialState,
  reducers: {
    logout: () => assetsinitialState,
    setAssets: (state, { payload }) => {
      state.token = payload.access_token;
      state.assets = payload;
    },
  },
  extraReducers: {},
});

export default assetsSlice.reducer;
export const { logout, setAssets } = assetsSlice.actions;
