import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";



export const authAPI = createApi({
  reducerPath: "authentication",
  tagTypes: ["Token"],
  baseQuery: fetchBaseQuery({
    baseUrl: process.env.REACT_APP_STONKY_SERVICE_API_HOST,
    credentials: "include",
  }),
  prepareHeaders: async (headers, { getState }) => {
    const token = await getState().auth.token;

    if (token) {
      headers.set("authorization", `Bearer ${token}`);
    }

    return headers;
  },
  endpoints: (builder) => ({
    login: builder.mutation({
      query: (info) => {
        let formData = null;
        if (info instanceof HTMLElement) {
          formData = new FormData(info);
        } else {
          formData = new FormData();
          formData.append("username", info.username);
          formData.append("password", info.password);
        }
        return {
          url: "/token",
          method: "post",
          body: formData,
          credentials: "include",
        };
      },
      invalidatesTags: ["Token"],
    }),
    getToken: builder.query({
      query: () => ({
        url: "/token",
        credentials: "include",
      }),
      providesTags: ["Token"],
    }),
    signup: builder.mutation({
      query: (data) => {
        return {
          url: "/users",
          body: data,
          method: "post",
          credentials: "include",
        };
      },
      invalidatesTags: ["Token"],
    }),
    logoutUser: builder.mutation({
      query: () => ({
        url: "/token",
        method: "delete",
        credentials: "include",
      }),
      invalidatesTags: ["Token"],
    }),
    editUser: builder.mutation({
      query: ({ users_id, data }) => ({
        url: `/users/${users_id}`,
        method: "put",
        body: data,
        credentials: "include",
      }),
      invalidatesTags: ["Token"],
    }),
  }),
});

export const {
  useLoginMutation,
  useGetTokenQuery,
  useSignupMutation,
  useLogoutUserMutation,
  useEditUserMutation,
} = authAPI;
