import { configureStore } from "@reduxjs/toolkit";
import { setupListeners } from "@reduxjs/toolkit/dist/query";
import { authAPI } from "./authAPI";
import { assetsAPI } from "./assetsAPI";
import { usersSlice } from "./usersSlice";
import { tradesAPI } from "./tradesAPI";
import { usersAPI } from "./usersAPI";


export const store = configureStore({
  reducer: {
    auth: usersSlice.reducer,
    [authAPI.reducerPath]: authAPI.reducer,
    [tradesAPI.reducerPath]: tradesAPI.reducer,
    [assetsAPI.reducerPath]: assetsAPI.reducer,
    [usersAPI.reducerPath]: usersAPI.reducer,
  },
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware()
      .concat(authAPI.middleware)
      .concat(tradesAPI.middleware)
      .concat(assetsAPI.middleware)
      .concat(usersAPI.middleware),
});



setupListeners(store.dispatch);

