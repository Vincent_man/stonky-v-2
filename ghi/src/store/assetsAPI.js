import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";
import { setAssets } from "./assetsSlice";

export const assetsAPI = createApi({
  reducerPath: "assets",
  tagTypes: ["BuySellAssets"],
  baseQuery: fetchBaseQuery({
    baseUrl: process.env.REACT_APP_STONKY_SERVICE_API_HOST,
    credentials: "include",
    prepareHeaders: async (headers, { getState }) => {
      const token = await getState().auth.token;

      if (token) {
        headers.set("authorization", `Bearer ${token}`);
      }

      return headers;
    },
  }),
  endpoints: (builder) => ({
    getAssets: builder.query({
      query: () => ({
        url: "/assets",
        credentials: "include",
      }),
      providesTags: ["BuySellAssets"],
      async onQueryStarted(_, { dispatch, queryFulfilled }) {
        try {
          const { data } = await queryFulfilled;
          dispatch(setAssets(data));
        } catch (e) {
          console.error(e);
        }
      },
    }),
    createAssets: builder.mutation({
      query: (info) => ({
        url: "/assets",
        method: "post",
        body: info,
      }),
      invalidatesTags: ["BuySellAssets"],
    }),
    UpdateAssets: builder.mutation({
      query: ({asset_id, info}) => ({
        url: `/assets/${asset_id}`,
        method: "put",
        body: info,
      }),
      invalidatesTags: ["BuySellAssets"],
    }),
    deleteAsset: builder.mutation({
      query: (user_id) => ({
        url: `/users/${user_id}/assets`,
        method: "DELETE",
        credentials: "include",
      }),
      invalidatesTags: ["BuySellAssets"],
    }),
    getOneAsset: builder.query({
      query: (asset_id) => ({
        url: `/assets/${asset_id}`,
        method: "get",
    }),
  }),
  }),
});

export const {
  useGetAssetsQuery,
  useCreateAssetsMutation,
  useUpdateAssetsMutation,
  useDeleteAssetMutation,
  useGetOneAssetQuery
} = assetsAPI;
