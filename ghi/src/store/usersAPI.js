import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";

export const usersAPI = createApi({
  reducerPath: "users",
  tagTypes: ["users"],
  baseQuery: fetchBaseQuery({
    baseUrl: process.env.REACT_APP_STONKY_SERVICE_API_HOST,
    credentials: "include",
  }),
  endpoints: (builder) => ({
    getUsers: builder.query({
      query: () => "/users",
      providesTags: ["users"],
    }),
    deleteUser: builder.mutation({
      query: (users_id) => ({
        url: `/users/${users_id}`,
        method: "delete",
      }),
      invalidatesTags: ["users"],
    }),
  }),
});

export const { useGetUsersQuery, useDeleteUserMutation } = usersAPI;
