import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";

export const tradesAPI = createApi({
  reducerPath: "trades",
  tagTypes: ["Trades"],
  baseQuery: fetchBaseQuery({
    baseUrl: process.env.REACT_APP_STONKY_SERVICE_API_HOST,
    credentials: "include",
    prepareHeaders: async (headers, { getState }) => {
      const token = await getState().auth.token;

      if (token) {
        headers.set("authorization", `Bearer ${token}`);
      }

      return headers;
    },
  }),
  endpoints: (builder) => ({
    getTrade: builder.query({
      query: () => ({
        url: "/trades",
      }),
    }),
    addTrade: builder.mutation({
      query: (trade) => ({
        url: "/trades",
        method: "post",
        body: trade,
      }),
    }),
    deleteTrade: builder.mutation({
      query: (user_id) => ({
        url: `/users/${user_id}/trades`,
        method: "delete",

        credentials: "include",
      }),
      invalidatesTags: ["Trades"],
    }),
  }),
});

export const {
  useGetTradeQuery,
  useAddTradeMutation,
  useDeleteTradeMutation
} = tradesAPI;
