import { createSlice } from '@reduxjs/toolkit';

const tradesSlice = createSlice({
  name: 'trades',
  initialState: {
    buyEvents: [],
    sellEvents: [],
    cashSpent: 0,
    cashEarned: 0,
    portfolioTotal: 0,
    profit: 0,
    totalLost: 0,
  },
  reducers: {
    addTrade(state, action) {
      state.buyEvents.push(action.payload);
    },
    setTrades(state, action) {
      state.buyEvents = action.payload.buyEvents;
      state.sellEvents = action.payload.sellEvents;
      state.cashSpent = action.payload.cashSpent;
      state.cashEarned = action.payload.cashEarned;
      state.portfolioTotal = action.payload.portfolioTotal;
      state.profit = action.payload.profit;
      state.totalLost = action.payload.totalLost;
    },
  },
});


export default tradesSlice.reducer;
export const { addTrade, setTrades } = tradesSlice.actions;
