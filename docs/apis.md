# APIS
---
## Assets
---
- **Method**: `GET`, `POST`, `GET`, `PUT`, `DELETE`
- **Path**: ghi\src\store\assetsAPI.js

Input:

```
{
  "ticker": "string",
  "amount": "string",
  "purchase_date": "2023-04-26",
  "purchase_price": "string",
  "sold_price": "string",
  "cost_basis": "string",
  "users_id": 0
}
```

Output:

```
{
  "id": 0,
  "ticker": "string",
  "amount": "string",
  "purchase_date": "2023-04-26",
  "purchase_price": "string",
  "sold_price": "string",
  "cost_basis": "string",
  "users_id": 0
}
```

Creating a new asset creates a unique id and saves the ticker, amount, purchase_date, purchase_price, sold_price, cost_basis, users_id. This adds this asset to the database to keep track of what asset(s) a user is holding.
## Auth
---
- **Method**: `GET`, `POST`, `DELETE`
- **Path**: ghi\src\store\authAPI.js

Input:

```
{
  "username": "string",
  "password: "string"
}

```

Output:

```
{
  "access_token": "string",
  "token_type": "Bearer"
}

```
Logging in will allow for an existing user to login using their username and password, the access _token and token_type they recieved after creating their account will verify that they are in our database. After logging in, the user will be able to access the profile page and the buy/sell component as well as the trading history component on the trading page.
## Trades
---
- **Method**: `GET`, `POST`, `GET`
- **Path**: ghi\src\store\tradesAPI.js

Input:

```
{
  "ticker": "string",
  "amount": "string",
  "purchase_date": "2023-04-26",
  "purchase_price": "string",
  "sold_price": "string",
  "cost_basis": "string",
  "users_id": 0
}
```

Output:

```
{
  "id": 0,
  "ticker": "string",
  "amount": "string",
  "purchase_date": "2023-04-26",
  "purchase_price": "string",
  "sold_price": "string",
  "cost_basis": "string",
  "users_id": 0
}
```

Creating a trade will create a unique id to that user and save the ticker, amount, purchase_date, purchase_price, sold_price, cost_basis and the unique users_id. This adds to our database to keep track of the trades a user has made to feed into the trade history component.
## Users
---
- **Method**: `GET`, `POST`, `PUT`, `DELETE`, `GET`
- **Path**: ghi\src\store\usersAPI.js

Input:

```
{
  "name": "string",
  "email": "string",
  "password": "string",
  "username": "string"
}
```

Output:

```
{
  "access_token": "string",
  "token_type": "Bearer",
  "account": {
    "id": 0,
    "name": "string",
    "email": "string",
    "hashed_password": "string",
    "username": "string"
  }
}
```

Creating a new user will save an access_token, token_type then a unique id, name, email, hashed_password and username under the account. This access token and token_type will then be used once the user tries to login with their username and password to verify that the user is in our database.