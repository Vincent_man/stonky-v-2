# User Graphical Human Interface
---
## Landing Page
---
This will be the home page that greets our visitors and users. There will be education cards that display the different levels of education we offer on this page.


![](LandingPage.png)

## Sign up
---
This page will allow visitors to create an account in order to invest on stonky. They will have to fill in a name, email and create a username and password.


![](Signup.png)

## Login
---
This page will allow current users to sign into their existing accounts by providing their email and password.


![](Login.png)

## Profile
---
This page will display only when a user is signed in, it allows a user to edit their account information such as:
* email
* password
* first name
The Profile page will also allow existing users to delete their account and display a ledger that features the user's bank/cash balance, money spent and money earned. The user also has the ability to reset their assets from this page utilizing the reset account button.


![](Profile.png)

## Trading
---
This page allows both users and visitors to view stocks through a search bar, the ticker that has been searched for will display in the form of a chart. Users and visitors may also view the leaderboard of the top investors in the stock market who are utilizing stoNky.
A buy/sell table is also displayed to authorized users in order for them to buy stocks or sell stocks they currently are holding.
A trade history table is displayed to authorized users as well keeping track of their past buys, past sells and their profits.



![](Trading.png)