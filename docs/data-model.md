# Data models
---

## stoNky microservice
---


## Users
```
| name        | type                        | unique | optional |
| ----------- | --------------------------- | ------ | -------- |
| id          | reference to Users entity | yes    | no       |
| name        | reference to Users entity | no    | no       |
| email       | reference to Users entity | yes     | no       |
| password    | reference to Users entity | no    | no       |
| username    | reference to Users entity | yes     | no       |
```
The `users` entity contains data about the user in order to verify them in our database.

## Assets
```
| name        | type                        | unique | optional |
| ----------- | --------------------------- | ------ | -------- |
| id          | reference to Assets entity | yes    | no       |
| ticker      | reference to Assets entity | no    | no       |
| amount      | reference to Assets entity | no     | no       |
| purchase_date| reference to Assets entity | no    | no       |
| purchase_price| reference to Assets entity | no     | no       |
| sold_price  | reference to Assets entity | no     | no       |
| cost_basis  | reference to Assets entity | no     | no       |
| users_id    | reference to Assets entity | yes     | no       |
```
The `assets` entity contains data about specific assets that a user has purchased.

## Trades
```
| name        | type                        | unique | optional |
| ----------- | --------------------------- | ------ | -------- |
| id          | reference to Trades entity | yes    | no       |
| ticker      | reference to Trades entity | no    | no       |
| amount      | reference to Trades entity | no     | no       |
| purchase_date| reference to Trades entity | no    | no       |
| purchase_price| reference to Trades entity | no     | no       |
| sold_price  | reference to Trades entity | no     | no       |
| cost_basis  | reference to Trades entity | no     | no       |
| users_id    | reference to Trades entity | yez     | no       |
```
The `trades` entity contains data about any trades a user has made.