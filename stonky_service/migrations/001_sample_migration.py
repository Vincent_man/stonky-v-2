steps = [
    [
        """
        CREATE TABLE users (
            id SERIAL PRIMARY KEY NOT NULL,
            name VARCHAR(50) NOT NULL,
            email VARCHAR(100) UNIQUE NOT NULL,
            password VARCHAR(100000) NOT NULL,
            username VARCHAR(30) UNIQUE NOT NULL
        )
        """,
        """
        DROP TABLE users;
        """
    ],

    [
        # Create the table
        """
        CREATE TABLE assets (
            id SERIAL PRIMARY KEY NOT NULL,
            ticker VARCHAR(4) NOT NULL,
            amount VARCHAR(1000) NOT NULL,
            purchase_date DATE NOT NULL,
            purchase_price VARCHAR(100) NOT NULL,
            sold_price VARCHAR(100) NOT NULL,
            cost_basis VARCHAR(1000),
            users_id INTEGER REFERENCES users(id)

        );
        """,
        # Drop the table
        """
        DROP TABLE assets;
        """

    ],
    [
        """
        CREATE TABLE trades (
            id SERIAL PRIMARY KEY NOT NULL,
            ticker VARCHAR(4) NOT NULL,
            amount VARCHAR(1000) NOT NULL,
            purchase_date DATE NOT NULL,
            purchase_price VARCHAR(100) NOT NULL,
            sold_price VARCHAR(100) NOT NULL,
            cost_basis VARCHAR(1000),
            users_id INTEGER REFERENCES users(id)
        )
        """,
        """
        DROP TABLE trades;
        """
    ],



]
