from fastapi import APIRouter, Response, Depends
from typing import List, Optional, Union
from queries.trades import (Error, TradesIn, TradesOut, TradesRepo)
from authenticator import authenticator


router = APIRouter()


@router.post("/trades", response_model=Union[TradesOut, Error])
def create_trade(
    trade: TradesIn,
    response: Response,
    repo: TradesRepo = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    if not account_data:
        response.status_code = 401
        return Error(message="you cant be here")
    response.status_code = 200
    return repo.create(trade)


@router.get("/trades", response_model=Union[List[TradesOut], Error])
def get_all(
    response: Response,
    repo: TradesRepo = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    if not account_data:
        response.status_code = 401
        return Error(message="you cant be here")
    return repo.get_all()


@router.get("/trades/{trades_id}", response_model=Optional[TradesOut])
def get_one_trade(
    trade_id: int,
    response: Response,
    repo: TradesRepo = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
) -> TradesOut:

    if not account_data:
        response.status_code = 401
        return Error(message="you cant be here")
    trade = repo.get_one(trade_id)
    if trade is None:
        response.status_code = 404
    return trade


@router.delete("/users/{user_id}/trades", response_model=bool)
def delete_all_user_trades(
    user_id: int,
    response: Response,
    repo: TradesRepo = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
) -> bool:
    if account_data["id"] == user_id:
        response.status_code = 200
        return repo.delete_all(user_id)
    elif not account_data:
        response.status_code = 401
        return Error(message="no account")
