from fastapi import (APIRouter,
                     Depends,
                     Response,
                     HTTPException,
                     status,
                     Request)


from jwtdown_fastapi.authentication import Token
from queries.users import (Error,
                           UsersIn,
                           UsersOut,
                           UsersRepository,
                           DuplicateAccountError)
from typing import Union, List
from authenticator import authenticator
from pydantic import BaseModel


class AccountForm(BaseModel):
    username: str
    password: str


class AccountToken(Token):
    account: UsersOut


class HttpError(BaseModel):
    detail: str


router = APIRouter()


@router.get("/token", response_model=AccountToken | None)
async def get_token(
    request: Request,
    account: UsersOut = Depends(authenticator.try_get_current_account_data)
) -> AccountToken | None:
    if account and authenticator.cookie_name in request.cookies:
        return {
            'access_token': request.cookies[authenticator.cookie_name],
            'token_type': 'Bearer',
            'account': account,
        }


@router.post("/users", response_model=AccountToken | HttpError)
async def create_users(
    users: UsersIn,
    request: Request,
    response: Response,
    repo: UsersRepository = Depends(),
):
    hashed_password = authenticator.hash_password(users.password)
    try:
        account = repo.create(users, hashed_password)
    except DuplicateAccountError:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Account already exists.",
        )

    form = AccountForm(username=users.username, password=users.password)
    token = await authenticator.login(response, request, form, repo)
    return AccountToken(account=account, **token.dict())


@router.put("/users/{users_id}", response_model=Union[UsersOut, Error])
def update_user(
    users_id: int,
    users: UsersIn,
    repo: UsersRepository = Depends(),
):
    hashed_password = authenticator.hash_password(users.password)
    return repo.update(users_id, users, hashed_password)


@router.get("/users/{username}", response_model=Union[UsersOut, Error])
def get_user(
    username: str,
    response: Response,
    repo: UsersRepository = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    if not account_data:
        response.status_code = 401
        return Error(message="you cant be here")
    response.status_code = 200
    return repo.get_one(username)


@router.get("/users", response_model=Union[List[UsersOut], Error])
def get_all_users(
    response: Response,
    repo: UsersRepository = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    if not account_data:
        response.status_code = 401
        return Error(message="you cant be here")
    response.status_code = 200
    return repo.get_all()


# @router.get("/users", response_model=Union[List[UsersLimitedOut], Error])
# def get_all_users_limited(
#     response: Response,
#     repo: UsersRepository = Depends()
# ):
#     response = repo.get_all()
#     if not response:
#         response.status_code = 400
#     return response


@router.delete("/users/{users_id}", response_model=bool)
def delete_users(
    users_id: int,
    response: Response,
    repo: UsersRepository = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
) -> bool:
    if not account_data:
        response.status_code = 401
        return Error(message="you cant be here")
    return repo.delete(users_id)
