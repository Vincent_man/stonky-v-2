from fastapi import APIRouter, Response, Depends
from typing import List, Optional, Union
from queries.assets import (Error, AssetsIn, AssetsOut, AssetsRepo)
from authenticator import authenticator

router = APIRouter()


@router.post("/assets", response_model=Union[AssetsOut, Error])
async def create_asset(
    asset: AssetsIn,
    response: Response,
    repo: AssetsRepo = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    if not account_data:
        response.status_code = 401
        return Error(message="you cant be here")
    response.status_code = 200
    return repo.create(asset)


@router.get("/assets", response_model=Union[List[AssetsOut], Error])
def get_all(
    response: Response,
    repo: AssetsRepo = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    if not account_data:
        response.status_code = 401
        return Error(message="you cant be here")
    response.status_code = 200
    return repo.get_all()


@router.get("/assets/{asset_id}", response_model=Optional[AssetsOut])
def get_one_asset(
    asset_id: int,
    response: Response,
    repo: AssetsRepo = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
) -> AssetsOut:
    if not account_data:
        response.status_code = 401
        return Error(message="you cant be here")
    asset = repo.get_one(asset_id)
    if asset is None:
        response.status_code = 404
    return asset


@router.delete("/users/{user_id}/assets", response_model=bool)
def delete_all_user_assets(
    user_id: int,
    response: Response,
    repo: AssetsRepo = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
) -> bool:
    if account_data["id"] == user_id:
        response.status_code = 200
        return repo.delete(user_id)
    elif not account_data:
        response.status_code = 401
        return Error(message="you can't be here")


@router.put("/assets/{asset_id}", response_model=Union[AssetsOut, Error])
def update_asset(
    asset_id: int,
    asset: AssetsIn,
    response: Response,
    repo: AssetsRepo = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    if not account_data:
        response.status_code = 401
        return Error(message="you cant be here")
    asset = repo.update(asset_id, asset)
    response.status_code = 200
    return asset
