from pydantic import BaseModel
from typing import List, Optional, Union
from datetime import date
from queries.pool import pool


class Error(BaseModel):
    message: str


class AssetsIn(BaseModel):
    ticker: str
    amount: str
    purchase_date: date
    purchase_price: str
    sold_price: str
    cost_basis: str
    users_id: int


class AssetsOut(BaseModel):
    id: int
    ticker: str
    amount: str
    purchase_date: date
    purchase_price: str
    sold_price: str
    cost_basis: str
    users_id: int


class AssetsRepo:
    def get_all(self) -> Union[Error, List[AssetsOut]]:
        try:
            # connect the database
            with pool.connection() as conn:
                # get a cursor (something to run SQL with)
                with conn.cursor() as db:
                    # Run our SELECT statement
                    result = db.execute(
                        """
                        SELECT id
                             , ticker
                             , amount
                             , purchase_date
                             , purchase_price
                             , sold_price
                             , cost_basis
                             , users_id
                        FROM assets
                        ORDER BY purchase_date;
                        """
                    )
                    return [
                        self.record_to_asset_out(record)
                        for record in result
                    ]
        except Exception as e:
            print(e)
            return {"message": "Could not get all assets"}

    def get_one(self, asset_id: int) -> Optional[AssetsOut]:
        try:
            # connect the database
            with pool.connection() as conn:
                # get a cursor (something to run SQL with)
                with conn.cursor() as db:
                    # Run our SELECT statement
                    result = db.execute(
                        """
                        SELECT id
                             , ticker
                             , amount
                             , purchase_date
                             , purchase_price
                             , sold_price
                             , cost_basis
                             , users_id
                        FROM assets
                        WHERE id = %s
                        """,
                        [asset_id]
                    )
                    record = result.fetchone()

                    if record is None:
                        return None
                    return self.record_to_asset_out(record)
        except Exception as e:
            print(e)
            return {"message": "Could not get that asset"}

    def create(self, asset: AssetsIn) -> Union[AssetsOut, Error]:
        try:
            # connect the database
            with pool.connection() as conn:
                # get a cursor (something to run SQL with)
                with conn.cursor() as db:
                    # Run our INSERT statement
                    result = db.execute(
                        """
                        INSERT INTO assets
                            (ticker,
                            amount,
                            purchase_date,
                            purchase_price,
                            sold_price,
                            cost_basis,
                            users_id)
                        VALUES
                            (%s, %s, %s, %s, %s, %s, %s)
                        RETURNING id;
                        """,
                        [
                            asset.ticker,
                            asset.amount,
                            asset.purchase_date,
                            asset.purchase_price,
                            asset.sold_price,
                            asset.cost_basis,
                            asset.users_id,
                        ]
                    )
                    id = result.fetchone()[0]
                    return self.asset_in_to_out(id, asset)
        except Exception:
            return {"message": "Create did not work"}

    def update(self,
               asset_id: int,
               asset: AssetsIn) -> Union[AssetsOut, Error]:
        try:
            # connect the database
            with pool.connection() as conn:
                # get a cursor (something to run SQL with)
                with conn.cursor() as db:
                    db.execute(
                        """
                        UPDATE assets
                        SET ticker = %s
                            , amount = %s
                            , purchase_date = %s
                            , purchase_price = %s
                            , sold_price = %s
                            , cost_basis = %s
                            , users_id = %s
                        WHERE id = %s
                        """,
                        [
                            asset.ticker,
                            asset.amount,
                            asset.purchase_date,
                            asset.purchase_price,
                            asset.sold_price,
                            asset.cost_basis,
                            asset.users_id,
                            asset_id
                        ]
                    )
                    return self.asset_in_to_out(asset_id, asset)
        except Exception as e:
            print(e)
            return {"message": "Could not update that asset"}

    def delete(self, user_id: int) -> bool:
        try:
            # connect the database
            with pool.connection() as conn:
                # get a cursor (something to run SQL with)
                with conn.cursor() as db:
                    db.execute(
                        """
                        DELETE FROM assets
                        WHERE users_id = %s
                        """,
                        [user_id]
                    )
                    return True
        except Exception as e:
            print(e)
            return False

    def asset_in_to_out(self, id: int, asset: AssetsIn):
        old_data = asset.dict()
        return AssetsOut(id=id, **old_data)

    def record_to_asset_out(self, record):
        return AssetsOut(
            id=record[0],
            ticker=record[1],
            amount=record[2],
            purchase_date=record[3],
            purchase_price=record[4],
            sold_price=record[5],
            cost_basis=record[6],
            users_id=record[7],
        )
