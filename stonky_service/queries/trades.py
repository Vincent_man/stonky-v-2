from pydantic import BaseModel
from typing import List, Optional, Union
from datetime import date
from queries.pool import pool


class Error(BaseModel):
    message: str


class TradesIn(BaseModel):
    ticker: str
    amount: str
    purchase_date: date
    purchase_price: str
    sold_price: str
    cost_basis: str
    users_id: int


class TradesOut(BaseModel):
    id: int
    ticker: str
    amount: str
    purchase_date: date
    purchase_price: str
    sold_price: str
    cost_basis: str
    users_id: int


class TradesRepo:
    def get_all(self) -> Union[Error, List[TradesOut]]:
        try:
            # connect the database
            with pool.connection() as conn:
                # get a cursor (something to run SQL with)
                with conn.cursor() as db:
                    # Run our SELECT statement
                    result = db.execute(
                        """
                        SELECT id
                             , ticker
                             , amount
                             , purchase_date
                             , purchase_price
                             , sold_price
                             , cost_basis
                             , users_id
                        FROM trades
                        ORDER BY purchase_date;
                        """
                    )
                    return [
                        self.record_to_trades_out(record)
                        for record in result
                    ]
        except Exception as e:
            print(e)
            return {"message": "Could not get all trades"}

    def get_one(self, trades_id: int) -> Optional[TradesOut]:
        try:
            # connect the database
            with pool.connection() as conn:
                # get a cursor (something to run SQL with)
                with conn.cursor() as db:
                    # Run our SELECT statement
                    result = db.execute(


                        # """
                        # SELECT a.*, t.*
                        # FROM assets a
                        # JOIN trades t ON a.users_id = t.users_id
                        # WHERE a.users_id = <user_id>;
                        """
                        SELECT id
                             , ticker
                             , amount
                             , purchase_date
                             , purchase_price
                             , sold_price
                             , cost_basis
                             , users_id
                        FROM trades
                        WHERE id = %s
                        """,
                        [trades_id]
                    )
                    record = result.fetchone()

                    if record is None:
                        return None
                    return self.record_to_trades_out(record)
        except Exception as e:
            print(e)
            return {"message": "Could not get that trades"}

    def create(self, trades: TradesIn) -> Union[TradesOut, Error]:
        try:
            # connect the database
            with pool.connection() as conn:
                # get a cursor (something to run SQL with)
                with conn.cursor() as db:
                    # Run our INSERT statement
                    result = db.execute(
                        """
                        INSERT INTO trades
                            (ticker,
                            amount,
                            purchase_date,
                            purchase_price,
                            sold_price,
                            cost_basis,
                            users_id)
                        VALUES
                            (%s, %s, %s, %s, %s, %s, %s)
                        RETURNING id;
                        """,
                        [
                            trades.ticker,
                            trades.amount,
                            trades.purchase_date,
                            trades.purchase_price,
                            trades.sold_price,
                            trades.cost_basis,
                            trades.users_id
                        ]
                    )
                    id = result.fetchone()[0]
                    return self.trades_in_to_out(id, trades)
        except Exception:
            return {"message": "Create did not work"}

    def update(self,
               trade_id: int,
               trade: TradesIn) -> Union[TradesOut, Error]:
        try:
            # connect the database
            with pool.connection() as conn:
                # get a cursor (something to run SQL with)
                with conn.cursor() as db:
                    db.execute(
                        """
                        UPDATE trades
                        SET ticker = %s
                            , amount = %s
                            , purchase_date = %s
                            , purchase_price = %s
                            , sold_price = %s
                            , cost_basis = %s
                            , users_id = %s
                        WHERE id = %s
                        """,
                        [
                            trade.ticker,
                            trade.amount,
                            trade.purchase_date,
                            trade.purchase_price,
                            trade.sold_price,
                            trade.cost_basis,
                            trade.users_id,
                            trade_id
                        ]
                    )
                    return self.trades_in_to_out(trade_id, trade)
        except Exception as e:
            print(e)
            return {"message": "Could not update that trade"}

    def delete(self, user_id: int) -> bool:
        try:
            # connect the database
            with pool.connection() as conn:
                # get a cursor (something to run SQL with)
                with conn.cursor() as db:
                    db.execute(
                        """
                        DELETE FROM trades
                        WHERE id = %s
                        """,
                        [user_id]
                    )
                    return True
        except Exception as e:
            print(e)
            return False

    def delete_all(self, user_id: int) -> bool:
        try:
            # connect the database
            with pool.connection() as conn:
                # get a cursor (something to run SQL with)
                with conn.cursor() as db:
                    db.execute(
                        """
                        DELETE FROM trades
                        WHERE users_id = %s
                        """,
                        [user_id]
                    )
                    return True
        except Exception as e:
            print(e)
            return False

    def trades_in_to_out(self, id: int, trade: TradesIn):
        old_data = trade.dict()
        return TradesOut(id=id, **old_data)

    def record_to_trades_out(self, record):
        return TradesOut(
            id=record[0],
            ticker=record[1],
            amount=record[2],
            purchase_date=record[3],
            purchase_price=record[4],
            sold_price=record[5],
            cost_basis=record[6],
            users_id=record[7],
        )
