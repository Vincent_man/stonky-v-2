from pydantic import BaseModel
from typing import List, Union
from queries.pool import pool


class UsersIn(BaseModel):
    name: str
    email: str
    password: str
    username: str


class UsersOut(BaseModel):
    id: int
    name: str
    email: str
    hashed_password: str
    username: str


class UsersLimitedOut(BaseModel):
    id: int
    name: str
    username: str
    email: str


class Error(BaseModel):
    message: str


class DuplicateAccountError(ValueError):
    pass


class AccountOutWithPassword(UsersOut):
    hashed_password: str


class UsersRepository:

    def __init__(self):
        self.users = []

    def add_user(self, user):
        self.users.append(user)

    def get(self, username):
        for user in self.users:
            if user.username == username:
                return user
        return None

    def get_one(self, username: str) -> AccountOutWithPassword:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT id
                        , name
                        , email
                        , password
                        , username

                        FROM users
                        WHERE username = %s;
                        """,
                        [username]
                    )
                    record = result.fetchone()
                    if record is None:
                        return None
                    return self.record_to_user_out(record)
        except Exception as e:
            print(e)
            return {"message": "Could not get user record"}

    def get_all(self) -> Union[Error, List[UsersOut]]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT id
                        , name
                        , email
                        , password
                        , username

                        FROM users;
                        """
                    )

                    records = result.fetchall()
                    return [self.record_to_user_out(record)
                            for record in records]
        except Exception as e:
            print(e)
            return {"message": "Could not get user records"}

    def delete(self, users_id: int) -> bool:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        DELETE FROM users
                        WHERE id = %s;
                        """,
                        [users_id]
                    )
                    return True
        except Exception as e:
            print(e)
            return False

    # def create(self, users: UsersIn) -> Union[UsersOut, Error]:
    def create(self,
               users: UsersIn,
               hashed_password: str) -> AccountOutWithPassword:
        try:
            # connect the database
            with pool.connection() as conn:
                # get a cursor (something to run SQL with)
                with conn.cursor() as db:
                    # Run our INSERT statement
                    result = db.execute(
                        """
                        INSERT INTO users
                            (name, email, password, username)
                        VALUES
                            (%s, %s, %s, %s)
                        RETURNING id;
                        """,
                        [
                            users.name,
                            users.email,
                            hashed_password,
                            users.username,
                        ]
                    )
                    id = result.fetchone()[0]
                    return self.user_in_to_out(id, hashed_password, users)

        except Exception:
            return {"message": "Create did not work"}

    def update(self,
               user_id: int,
               users: UsersIn,
               hashed_password: str) -> Union[UsersOut, Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        UPDATE users
                        SET name = %s, email = %s, password = %s, username = %s
                        WHERE id = %s
                        """,
                        [
                            users.name,
                            users.email,
                            hashed_password,
                            users.username,
                            user_id,
                        ]
                    )
                    return self.user_in_to_out(user_id, hashed_password, users)

        except Exception as e:
            (e)
            return {"message": "Update did not work"}

    def user_in_to_out(self, id: int, hashed_password: str, users: UsersIn):
        old_data = users.dict()
        return UsersOut(id=id,
                        hashed_password=hashed_password,
                        **old_data)

    def record_to_user_out(self, record):
        return UsersOut(
            id=record[0],
            name=record[1],
            email=record[2],
            hashed_password=record[3],
            username=record[4],
        )
