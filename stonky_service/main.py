from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
import os
from routers import assets, users, trades
from authenticator import authenticator

app = FastAPI()

app.include_router(authenticator.router)
app.include_router(assets.router)
app.include_router(users.router)
app.include_router(trades.router)

app.add_middleware(
    CORSMiddleware,
    allow_origins=[
        "http://localhost:3000",
        os.environ.get("CORS_HOST", None),
    ],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)
