from main import app
from authenticator import authenticator
from queries.assets import AssetsRepo
from fastapi.testclient import TestClient
from datetime import date

today = date.today()
formatted_date = today.strftime("%Y-%m-%d")

client = TestClient(app)
asset_id = 1


def test_asset():
    return {
        "ticker": "aaaa",
        "amount": "string",
        "purchase_date": formatted_date,
        "purchase_price": "string",
        "sold_price": "string",
        "cost_basis": "string",
        "users_id": 1
    }


class GetAssetQueries:
    def get_one(self, asset_id):
        return {
            "id": 1,
            "ticker": "aaaa",
            "amount": "string",
            "purchase_date": formatted_date,
            "purchase_price": "string",
            "sold_price": "string",
            "cost_basis": "string",
            "users_id": 1
        }


def test_create_asset():
    (app.dependency_overrides
        [authenticator.get_current_account_data]) = test_asset

    app.dependency_overrides[AssetsRepo] = GetAssetQueries
    expected = {
        "id": 1,
        "ticker": "aaaa",
        "amount": "string",
        "purchase_date": formatted_date,
        "purchase_price": "string",
        "sold_price": "string",
        "cost_basis": "string",
        "users_id": 1
        }

    response = client.get(f"/assets/{asset_id}")

    app.dependency_overrides = {}

    assert response.status_code == 200
    assert response.json() == expected


def test_init():
    assert 1 == 1
