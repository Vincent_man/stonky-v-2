from main import app
from authenticator import authenticator
from queries.users import UsersRepository
from fastapi.testclient import TestClient

client = TestClient(app)


def test_user():
    return {
        "id": 1,
        "name": "string",
        "email": "string",
        "password": "string",
        "username": "string"
    }


class GetUserQueries:
    def get_one(self, username):
        return {
            "id": 1,
            "name": "string",
            "email": "string",
            "hashed_password": "string",
            "username": username
        }


def test_get_user():
    # Arrange
    app.dependency_overrides[UsersRepository] = GetUserQueries
    (app.dependency_overrides
        [authenticator.get_current_account_data]) = test_user
    username = "string"
    expected = {
            "id": 1,
            "name": "string",
            "email": "string",
            "hashed_password": "string",
            "username": "string"
        }

    response = client.get(f"/users/{username}")

    app.dependency_overrides = {}

    assert response.status_code == 200
    assert response.json() == expected


def test_init():
    assert 1 == 1
