from main import app
from authenticator import authenticator
from queries.trades import TradesRepo
from fastapi.testclient import TestClient
from datetime import date

today = date.today()
formatted_date = today.strftime("%Y-%m-%d")

client = TestClient(app)


def testuser():
    return {
        "id": 1,
        "username": "test",
        "hashed_password": "test",
        "email": "test"
    }


class GetAllTradesQuery:
    def get_all(self):
        return [{
            "id": 1,
            "ticker": "string",
            "amount": "string",
            "purchase_date": formatted_date,
            "purchase_price": "string",
            "sold_price": "string",
            "cost_basis": "string",
            "users_id": 1
        }]


def test_get_all_trades():
    (app.dependency_overrides
        [authenticator.try_get_current_account_data]) = testuser
    app.dependency_overrides[TradesRepo] = GetAllTradesQuery

    expected = [
        {
            "id": 1,
            "ticker": "string",
            "amount": "string",
            "purchase_date": formatted_date,
            "purchase_price": "string",
            "sold_price": "string",
            "cost_basis": "string",
            "users_id": 1
        }]

    response = client.get("/trades")

    app.dependency_overrides = {}

    assert response.status_code == 200
    assert response.json() == expected


def test_init():
    assert 1 == 1
