from main import app
from authenticator import authenticator
from queries.assets import AssetsRepo
from fastapi.testclient import TestClient


client = TestClient(app)


def fake_acc():
    return {
        "id": 1,
        "name": "string",
        "username": "string",
        "email": "string",
    }


class AssetQuery:
    def delete(self, user_id):
        return True


def test_delete_asset():
    app.dependency_overrides[AssetsRepo] = AssetQuery
    (app.dependency_overrides
        [authenticator.get_current_account_data]) = fake_acc
    user_id = 1
    expected = True
    response = client.delete(f"/users/{user_id}/assets")
    app.dependency_overrides = {}
    assert response.status_code == 200
    assert response.json() == expected


def test_init():
    assert 1 == 1
